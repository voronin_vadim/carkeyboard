﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName ="Border" , menuName = "AddGameElem/AddBorder")]
public class ConfigBorder : ScriptableObject {

    public Sprite mainSprite;
    public Sprite secondSprite; 
}
