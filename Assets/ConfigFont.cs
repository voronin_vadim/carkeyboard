﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName ="Font" , menuName = "AddGameElem/AddFont")]
public class ConfigFont : ScriptableObject {

    public Font font;
    public string fontName;
    public int fontsize=25;

    public void OnValidate()
    {
        fontName = font.name;
    }

}
