﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Game2D.Config
{
   

    public class ConfigData : MonoSingleton<ConfigData>
    {

        //config font
        [Header("Font config")]
        [SerializeField]
        List<ConfigFont> fonts; 

        //config borders
        [Header("Border config")]
        [SerializeField]
        List<ConfigBorder> borders;

     
        //config borders   
        [Header("Car config")]
        [SerializeField]
        List<ConfigCar> carsData;


        public int GetRandom(ConfigType type)
        {
            ICollection targetNum = null; 
            switch (type)
            {
                case ConfigType.font:                 
                        targetNum =  fonts;
                        break;                    
                case ConfigType.borders:
                    targetNum =borders;
                        break;                    
                case ConfigType.cars:
                    targetNum = carsData;
                       
                        break;                   
            }
            return Random.Range(0,  targetNum?.Count ?? 0);            
        }

        public object GetConfigType(ConfigType type, int num)
        {
            try
            {
                switch (type)
                {
                    case ConfigType.font:                      
                            return(fonts[num]);
                         
                    case ConfigType.borders:                       
                            return borders[num];

                    case ConfigType.cars:                        
                            return carsData[num];    
                }
            }
            catch (System.Exception ex)
            {
                Debug.LogError("Error on GetConfigType  " + ex.ToString(), this);
                return null;
            }
            return null;
        }
    }



}
