﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Random = UnityEngine.Random;
using Game2D.Config; 

  namespace Game2D.game {
    public class PlayerBot : Player {


        public float minTemp = 60;
        public float maxTemp = 100;
        public float accurate = 80;

        float accurateTemp;

        float timerWait;

        DateTime exitTime = DateTime.MinValue;

        public override void InitPlayer()
        {
            inGame = true;
            StartCoroutine(StartWait());
        }

        public override void GameIsOver()
        {
            base.GameIsOver();
            //StopAllCoroutines();
        }

        public void StopType()
        {
            inGame = false;
        }

        void Send()
        {
            if (!inGame) return;
            accurateTemp = Random.Range(0, 100);
            SendSimbol(this, (accurate > accurateTemp).ToString()); //  for universal interface
        }

        public void OnApplicationPause(bool pause)
        {
            if (!pause) // simulate player Exit
            {
                if (exitTime == DateTime.MinValue) return;
                TimeSpan delta = System.DateTime.Now.Subtract(exitTime);
                int second = (int)delta.TotalSeconds;
                int step = (int)(second * (60f * 2f) / (minTemp + maxTemp));

                for (int i = 0; i < step; i++)
                    Send();
            }
            else
            {
                exitTime = DateTime.Now;
            }
        }


        IEnumerator WaitStepCoroutine()
        {
            timerWait = Random.Range(60f / maxTemp, 60f / minTemp);
            yield return new WaitForSeconds(timerWait);
            if (!inGame) yield break;
            Send();
            StartCoroutine(WaitStepCoroutine());


        } IEnumerator StartWait()
        {
            yield return new WaitForSeconds(Random.Range(3f, 5f));
            StartCoroutine(WaitStepCoroutine());
        }
    }
}
