﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Networking;

public abstract class Player : NetworkBehaviour
{

    public Action<Player, string> SendSimbol;
    protected bool inGame;

    [SyncVar(hook = "SyncChangeName")]
    public string playerGameName; 
    /// <summary>
    /// Game is over if any player do the job
    /// </summary>
    public virtual void GameIsOver()
    {
        inGame = false;
    } 

    public virtual void InitPlayer()
    {
    }

    public void SyncChangeName(string pName)
    {
        playerGameName = pName;
    }

}
