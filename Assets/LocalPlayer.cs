﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Game2D.Config;
using System;
using System.Linq; 

namespace Game2D.game {
    public class LocalPlayer : Player {

        Coroutine keyBoard;
        string lastSim;
        string lastText=""; 
        private TouchScreenKeyboard keyboard;
        public override void InitPlayer()
        {
            TouchScreenKeyboard.hideInput = true;
            ShowKeyBoard();
            inGame = true;
        }
        public  void ShowKeyBoard()
        {
            keyboard = TouchScreenKeyboard.Open("", TouchScreenKeyboardType.Default, false, false, false, false, "");
        }
        public override void GameIsOver()
        {
            base.GameIsOver();// in game = false 
            keyboard.active = false;
        }
       
        public void Update()
        {
            if (!inGame) return;
            /* if (Input.inputString != "")
             {

                 SendSimbol(this, Input.inputString.ToLower());
                 keyboard.text = "";
             }*/
            if (lastText.Length != Input.inputString.Length)
            {
                lastSim = Input.inputString.Last().ToString();
                SendSimbol(this, lastSim);
            }
            /*
            if (Input.anyKeyDown)
            {
                Debug.Log("KEY DOWN");
                lastSim = Input.inputString;            
                if (!String.IsNullOrEmpty(lastSim))
                {
                    lastSim = lastSim.Last().ToString();
                    SendSimbol(this, lastSim);
                }
            }*/

            /* if (keyboard.active == false)
             {
                 ShowKeyBoard();
                 keyboard.active = true;
             }*/

            if (keyboard.status != TouchScreenKeyboard.Status.Visible && keyBoard == null)
            {
                keyBoard = StartCoroutine(KeyBoardWait());
                ShowKeyBoard();
            }

        }
        IEnumerator KeyBoardWait()
        {
            yield return new WaitForSeconds(3f);
            keyBoard = null; 
                 
        }
    }
}
