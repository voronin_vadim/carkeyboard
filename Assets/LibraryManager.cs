﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Game2D.game {
    public class LibraryManager : MonoSingleton<LibraryManager> {


        [SerializeField] LanguagePackage enText;
        [SerializeField] LanguagePackage ruText;
        [SerializeField] LanguagePackage spText; 
        // Use this for initialization
        void Start() {

        }

        // Update is called once per frame
        void Update() {

        }

       public  string GetText()
        {
            string loca;
            string startString;
            loca = LocalizationService.Instance.Localization;
            switch (loca)
            {
                case "Russian":
                    {
                        startString = ruText.GetRandomText();
                        break; 
                    }
                case "Espanol":
                    {
                        startString = spText.GetRandomText();
                        break;
                    }
                default:
                    {
                        startString = enText.GetRandomText();
                    }
                    break; 
            }
            return startString; 
        }
    }
}
