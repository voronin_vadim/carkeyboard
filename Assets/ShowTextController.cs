﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.Text;
using DG.Tweening;
using System; 

public class ShowTextController  {

    [SerializeField]
    public Text gameTextCurrent;
     
    //-------  rish text ------ 
    const string RIGHT_SUF = "<color=green>";
    const string WRONG_SUF = "<color=red>";
    const string NEXT_SUF = "<color=black>";
    const string POST_FIX = "</color>";
    public  const  string LINE_BREAK = "\r\n";


    //ShowTextState
    List<int> brakeArray;
    int lastIndex;
    int currentLineShow;
    int showLineCount = 0;
    StringBuilder sb;
    string startString;


    public  ShowTextController(string text , Text textToShow)  //Programm start
    {
        gameTextCurrent = textToShow; 
        startString = text;
        sb = new StringBuilder(512);
        brakeArray = FindLineBreak(startString); //   find place where we have line break and store this place
        lastIndex = startString.Length;
        gameTextCurrent.text = text.ToUpper(); 
    }

    //find breake line position
    List<int> FindLineBreak(string startText)
    {
        var result = new List<int>();
        for (int i = 0; i < startText.Length; i++)
        {
            if (startText[i] == LINE_BREAK[0])
            {
                result.Add(i);
            }
        }
        return result;
    }

    // добавляем места чтобы корректно отображались переносы строк /r/n  точнее места под них
    void AddBrakeScope(ref List<int> progress)
    {
        int i = 0;
        int j = 0;
        int tempSumma = 0;

        int curentStep = 0;
        bool skipAdd = false;
        while (i < brakeArray.Count && j < progress.Count)
        {
            if (!skipAdd) tempSumma += progress[j];

            if (tempSumma >= brakeArray[i])
            {
                i++;
                tempSumma += 2;
                progress[j] += 2;
                skipAdd = true;
                continue;
            }
            skipAdd = false;
            j++;
        }
    }

    public void ShowTextByProgress(List<int> progress)
    {
        AddBrakeScope(ref progress);
        ShowText(progress);
    }


    // move Text line
    void UpdateLineShow(int line)
    {
        currentLineShow = line;
        int dif = (currentLineShow - showLineCount);
        if (dif <= 0) return;
        float fontSize = gameTextCurrent.fontSize;
        gameTextCurrent.GetComponent<RectTransform>().DOAnchorPosY(dif * fontSize, 1.5f);  // 1.5 line spacing
    }


    // show Text on local Player
    void ShowText(List<int> progress)
    {
        if (progress == null) return;
        sb.Clear();
        bool right = true;
        int tempSimbol = 0;
        for (int i = 0; i < progress.Count; i++)
        {
            sb.Append(right ? RIGHT_SUF : WRONG_SUF);
            if (!right)
                sb.Append(startString.Substring(tempSimbol, progress[i]).Replace(" ", "_"));
            else
                sb.Append(startString.Substring(tempSimbol, progress[i]));
            sb.Append(POST_FIX);
            tempSimbol += progress[i];
            right = !right;
        }
        sb.Append(NEXT_SUF);
        int nextSivol = progress.Sum();

        if (nextSivol >= lastIndex) return; 
        bool isSimvol = !Char.IsSymbol(startString[nextSivol]);

        while (nextSivol < lastIndex && LINE_BREAK.Contains(startString[nextSivol]))

        {
            nextSivol++;
            sb.Append(startString[nextSivol]);
        }
        sb.Append(startString[nextSivol]);
        sb.Append(POST_FIX);
        sb.Append(startString.Substring(nextSivol + 1));

        int currentLine = brakeArray.Count(p => p < nextSivol);
        if (currentLineShow != currentLine)
            UpdateLineShow(currentLine);
        gameTextCurrent.text = sb.ToString().ToUpper();
    }
}
