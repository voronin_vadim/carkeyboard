﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System.Linq;
using UnityEngine.UI; 

public class UiScriptOpenGrid : MonoBehaviour {

    public Button openButton;  
    RectTransform targetTransform; 
    Vector2 startSize;  
    float targetSize;

    bool select = false; 

	// Use this for initialization
	void Start () {
        targetTransform = GetComponent<RectTransform>(); 
        startSize = targetTransform.sizeDelta;
        if (openButton)
            openButton.onClick.AddListener(() => SelectGrid());  
    }
    public void SelectGrid()
    { float targetHeight; 
        select = !select;
        targetSize = (select) ? Enumerable.Range(0, targetTransform.childCount)
                .Select(p => targetTransform.GetChild(p).GetComponent<RectTransform>())
                .Sum(k => k.sizeDelta.y)
                : startSize.y;

        targetTransform.DOSizeDelta(new Vector2(startSize.x, targetSize), 1f); 
    }
	
}
