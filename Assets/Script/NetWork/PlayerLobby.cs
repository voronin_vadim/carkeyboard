﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;


namespace Game2D.Net
{
    public class PlayerLobby : NetworkLobbyPlayer  //Player lobby can't exist in Canvas because mast be DONT DEStroy .  Therefore add showScript
    {
        [Header("Show Player")]
        [SerializeField]
        PlayerLobbyShow playerShowPrefab;

        [SerializeField]
        PlayerLobbyShow playerGameShowPrefab;

        [SerializeField]
        PlayerLobbyShow playerShow;

        bool autoReady;

        [Header("Sync Data")]
        [SyncVar(hook = "SyncChangeCar")]
        public int spriteNum = 3;

        [SyncVar(hook = "SyncName")]
        public string playerName = "Player";

        [SyncVar(hook = "SyncChangeBorder")]
        public int borderNum = 0;



        // ---------------sync func --------------- 
        void SyncName(string name)// attach to SyncVar
        {
            playerName = name;
            SetName();
        }
        void SyncChangeCar(int num)   // attach to SyncVar
        {
            spriteNum = num;
            SetCar();
        }
        void SyncChangeBorder(int border)
        {
            borderNum = border;
            SetBorder();
        }


        // ------------setParam from data -------------------
        void SetCar()
        {
            ConfigCar configCar = (ConfigCar)Game2D.Config.ConfigData.Instance.GetConfigType(Game2D.Config.ConfigType.cars, spriteNum);
            playerShow.SetCar(configCar.mainSprite, configCar.fillColor);
        }
        void SetName()
        {
            playerShow.SetName(playerName);
        }
        void SetBorder()
        {
            Sprite border = ((ConfigBorder)Game2D.Config.ConfigData.Instance.GetConfigType(Game2D.Config.ConfigType.borders, borderNum)).mainSprite;
            playerShow.SetBorder(border);
        }


        public void GetStartParam() // local player
        {
            spriteNum = Config.ConfigManager.GetCurrentIndex(Config.ConfigType.cars);  //setCar         
            playerName = PlayerPrefs.GetString(Config.ConfigManager.NICK_NAME, "").ToString();  //setPlayer          
            borderNum = Config.ConfigManager.GetCurrentIndex(Config.ConfigType.borders);  //set border

            SetAllParam();
            CmdAllAsync();
        }


        // ---------- commands -------
        [Command]
        void CmdChangeBorder(int borderNumber)
        {
            borderNum = borderNumber;
        }

        [Command]  //  send to server 
        void CmdChangeCar(int carNumber)
        {
            spriteNum = carNumber;
        }

        [Command]  //  send to server 
        void CmdChangeName(string name)
        {
            playerName = name;
        }

        [Command]
        public void CmdExitToLobby()
        {
            var lobby = NetworkManager.singleton as LobbyManager;
            if (lobby != null)
            {
                lobby.ServerReturnToLobby();
            }
        }

        //  ----------------- server command -------------
        public override void OnClientEnterLobby()
        {
            base.OnClientEnterLobby();
            SetContexIsLobby(true);
            SetAllParam();
            autoReady = true;    // local player or not  determinate only in next frame     
        }
        public void CmdAllAsync()
        {
            CmdChangeName(playerName);
            CmdChangeCar(spriteNum);

            CmdChangeBorder(borderNum);
        }

        public void SetAllParam()
        {
            SetName();
            SetCar();
            SetBorder();
        }
        public void SetContexIsLobby(bool lobby)
        {
            if (playerShow != null)
                Destroy(playerShow);

            if (lobby)
            {
                playerShow = Instantiate(playerShowPrefab, ShowLobby.Instance.lobbyPlayerTransform);
                playerShow.transform.localScale = Vector3.one;
            }

            // else
            //   playerShow = Instantiate(playerGameShowPrefab, null);

        }



        private void Update()
        {
            if (autoReady)
            {
                autoReady = false;
                if ((isLocalPlayer) && (!isServer)) SendReadyToBeginMessage();
            }
        }


        public override void OnStartLocalPlayer()
        {
            GetStartParam();
            Debug.Log("OnStartLocalPlayer", this);
        }
        public override void OnClientExitLobby()
        {
            base.OnClientExitLobby();

        }

        public override void OnClientReady(bool readyState)
        {
            Debug.Log("Client ready " + readyState);
        }

        void OnGUIReady()
        {
            if (isLocalPlayer)
                SendReadyToBeginMessage();
        }

        void OnGUIRemove()
        {
            if (isLocalPlayer)
            {
                ClientScene.RemovePlayer(playerControllerId);
            }
        }
        private void OnDestroy()
        {

            if (playerShow != null)
                Destroy(playerShow.gameObject);
        }

    }
}
