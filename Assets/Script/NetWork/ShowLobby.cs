﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using UnityEngine.Networking.Match;
using System.Linq;
using UnityEngine.SceneManagement; 




namespace Game2D.Net
{

    public class ShowLobby : MonoBehaviour
    {
        public Transform lobbyPlayerTransform;

        public static ShowLobby Instance;

        [Header("Buttons start game Type")]
        [SerializeField]
        Button trainingGame;
        [SerializeField]
        Button fastGame;
        [SerializeField]
        Button matchGame;


        [Space(20)]
        public GameObject mainLobby;
        public Button cancelButton;
        public Button rejectButton;
        public Button startButton;
        public Button adPlayer;
        public GameObject waitForConnect;  // with text 
        public GameObject waitForFind;

        public GameObject waitForServerStart;
        public RectTransform waitForFindIcon;

        public InputField adress;

        public Button massageGo;
        private void Awake()
        {
            Instance = this;
        }
        // Use this for initialization
        void Start()
        {       

            Vector2 localPos = waitForFindIcon.anchoredPosition;
            float value = 40;
            Sequence sq = DOTween.Sequence();
            sq.Append(waitForFindIcon.DOAnchorPos(localPos + new Vector2(value, -value), 1f, true));
            sq.Append(waitForFindIcon.DOAnchorPos(localPos + new Vector2(0, -2 * value), 1f));
            sq.Append(waitForFindIcon.DOAnchorPos(localPos + new Vector2(-value, -value), 1f));
            sq.Append(waitForFindIcon.DOAnchorPos(localPos + new Vector2(0, 0), 1f));
            sq.SetLoops(-1);


            // -----------------------  game Type ----------------
            trainingGame?.onClick.AddListener(() => { Game2D.game.GameManager.localGame = true; SceneManager.LoadScene("game"); 
            });
            fastGame?.onClick.AddListener(() => { LobbyManager.l_Singleton.FindFastGame(); ShowAllButtonsType(false); });
            matchGame?.onClick.AddListener(() => {
                LobbyManager.l_Singleton.FindMatchGame();
                // ShowAllButtonsType(false);
            });




            Debug.Log("LevelLOad");
            cancelButton.gameObject.SetActive(false);
            startButton.gameObject.SetActive(false);
            cancelButton.onClick.AddListener(() => LobbyManager.l_Singleton.CloseMatch());
            startButton.onClick.AddListener(() => LobbyManager.l_Singleton.SendReadyHostPlayer());
            massageGo.onClick.AddListener(() => OpenLobby());
        }
        public void UpdateMatchCount(int count)
        {

            if (count>= 2) //   crutch for fast game
            {
                LobbyManager.l_Singleton.SendReadyHostPlayer(); 
                LobbyManager.l_Singleton.CloseMatchAccess();            
                
                //todo show wait for start
            }
        }
        public void ShowMassageGo()
        {
            if (Game2D.UI.Windows.Curren.Name != UI.NavigationButtonType.Map)
                massageGo.gameObject.SetActive(true);
        }
        void OpenLobby()
        {
            massageGo.gameObject.SetActive(false);
            Game2D.UI.Windows.ShowWindows(UI.NavigationButtonType.Map);
        }

        public void ShowAllButtonsType(bool show)
        {
            trainingGame.transform.parent.gameObject.SetActive(show);
        }
        public void OnMatchJoin()
        {
          waitForServerStart.SetActive(true);
          waitForFindIcon.gameObject.SetActive(false);

        }
      
        public void OnMatchCreate()
        {
          waitForFind.SetActive(true);
          cancelButton.gameObject.SetActive(true); 
        }

        public void StartLobby(bool start)
        {
           mainLobby.SetActive(start);
        }
    }
}
