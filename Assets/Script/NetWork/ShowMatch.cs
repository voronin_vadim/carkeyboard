﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking.Match;
using UnityEngine.Networking.Types;
using UnityEngine.UI;
using System.Text;
using System;


namespace Game2D.Net
{
    public enum ShowMatchState
    {
        wait, 
        join,
        play
    }
    public class ShowMatch : MonoBehaviour
    {  

        public Text numericText;
        public Text mName;
        public Text count;
        public Text attributeInfos;
        public GameObject passwordIcon; 
        [Header("Choice Buttons")]
        [SerializeField] Button play;
        [SerializeField] Button waitPlayer;
        [SerializeField] Button join;

       
        //private 
        bool withPasword = true; 
        NetworkID networkId;

        public Action<NetworkID, bool> onJoinMatch = delegate { };
        public Action onBrakeMatch = delegate { };
        public Action onPlayMatch = delegate { }; 
        

        private void Awake()
        {
            play?.onClick.AddListener(() => onPlayMatch.Invoke());
            waitPlayer?.onClick.AddListener(() => onBrakeMatch.Invoke());
            join?.onClick.AddListener(() => onJoinMatch.Invoke(networkId ,withPasword));
        }
        public void SetState(ShowMatchState state)
        {
            play.gameObject.SetActive(false); 
            waitPlayer.gameObject.SetActive(false);
            join.gameObject.SetActive(false);
            switch (state)
            {
                case ShowMatchState.join:
                    join.gameObject.SetActive(true); 
                    break;
                case ShowMatchState.play:
                    play.gameObject.SetActive(true);
                    break;
                case ShowMatchState.wait:
                    join.gameObject.SetActive(true);
                    break;
            }
        }
        public void SetEnable( NetworkID id )
        {
            join.gameObject.SetActive(id!= networkId);

        }
        
        public void Show(MatchInfoSnapshot info, int numeric)
        {
            gameObject.SetActive(true);

            string fullName = info.name;
            if (fullName.EndsWith("free"))
            {
                withPasword = false;
                fullName= fullName.Substring(0, fullName.Length - 4); // - free
            }
            if (mName) mName.text = fullName;
            passwordIcon?.SetActive(withPasword); 
            if (count) count.text = info.currentSize + "/" + info.maxSize;
            networkId = info.networkId;
            if (numericText) numericText.text = numeric.ToString() + ".";
            //  атрибуты всегда null
            if (info.matchAttributes == null) return;
            StringBuilder sb = new StringBuilder();
            foreach (var atr in info.matchAttributes)
            {
                sb.Append(atr.Key);
                sb.Append(atr.Value);
                sb.AppendLine();
            }
            if (attributeInfos) attributeInfos.text = sb.ToString();
        }
    }
}
