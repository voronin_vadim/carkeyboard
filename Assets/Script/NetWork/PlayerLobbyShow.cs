﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Game2D.Net
{
    public class PlayerLobbyShow : MonoBehaviour
    {
        public Graphic color; 
        public Image mainImage;
        public Text playerNameText;
        public Image borderImage;

        public string pName;

        private void Awake()
        {
            if (borderImage) borderImage.gameObject.SetActive(false); 
        }
        public void SetCar(Sprite carSprite, Color dColor)
        {
          if (mainImage)  mainImage.sprite = carSprite;
         if (color) color.color = dColor;
        }
        public void SetName(string playerName)
        {
         if (playerNameText)   playerNameText.text = playerName;
            pName = playerName; 
        }
        public void SetBorder(Sprite borderSprite)
        {
            if (!borderImage) return;
                borderImage.sprite = borderSprite;
            borderImage.gameObject.SetActive(true); 
        }
    }
}