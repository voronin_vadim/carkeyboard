﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;
using UnityEngine.Networking.Types;
using System;
using System.Linq;
using DG.Tweening;
using UnityEngine.SceneManagement;

namespace Game2D.Net
{

    public class LobbyManager : NetworkLobbyManager
    {
        enum MatchType
        {
            fast,
            team,
        }
        MatchType type;

        public string onlineStatus;
        public static LobbyManager l_Singleton
        {
            get
            {
                if (instance == null)
                {
                    instance = FindObjectOfType<LobbyManager>();
                }
                return instance;
            }
        }
        static LobbyManager instance;
        bool waitMassage = false;
        Coroutine waitPlayerCoroutine;
        MatchInfo currentMatchInfo;

        public void OnClickAddPlayer()
        {
            int id = NetworkClient.allClients[0].connection.playerControllers.Count;
            ClientScene.AddPlayer((short)id);
        }
        //start server 

        public void OnClickStartHost()
        {
            StartHost();
        }

        public void OnClickStartServer()
        {
            StartServer();
        }
        public void OnClickCencelConnect()
        {
            ShowLobby.Instance.cancelButton.gameObject.SetActive(false);
        }

        //  type   fast = 0 , team =1 ; 

        // ----------- fast game type ---------------
        public void FindFastGame()
        {
            Game2D.game.GameManager.localGame = false;

            if (!matchMaker)
                StartMatchMaker();
            ShowLobby.Instance.waitForFindIcon.gameObject.SetActive(true);
            matchMaker.ListMatches(0, 6, "", false, 0, 0, OnFindFastMatchList);
        }
        void OnFindFastMatchList(bool success, string extendedInfo, List<MatchInfoSnapshot> matches)
        {
            if (!success)
            {
                Debug.LogError("can't find Fast game " + extendedInfo);
                ShowLobby.Instance.ShowAllButtonsType(true);
                return;
            }
#if UNITY_EDITOR
            //  test show match info
            int index = 0;
            foreach (var match in matches)
            {
                Debug.Log($"Match find {index}  =  " +
                    $" <color =red> networkId { match.networkId}</color>  " +
                    $" <color =magenda> currentsize { match.currentSize}</color>  " +
                    $" <color =greed> name { match.name}</color>  "
                    );
                index++;
            }
#endif
            MatchInfoSnapshot last = matches?.LastOrDefault(p => p.maxSize > p.currentSize && p.currentSize > 0);
            if (last != null)
            {
                matchMaker.JoinMatch(last.networkId, "", "", "", 0, 0, OnMatchJoined);
            }
            else
            {
                string matchN = PlayerPrefs.GetString(Config.ConfigManager.NICK_NAME);
                matchMaker.CreateMatch(matchN, 4, true, "", "", "", 0, 0, OnMatchCreateFast);
                type = MatchType.fast;
            }
        }

        public void CloseMatchAccess()
        {
            matchMaker.SetMatchAttributes(currentMatchInfo.networkId, false, 0, (success, extendedInfo) => { }); // close current match to find 
        }
        void OnMatchCreateFast(bool success, string extendedInfo, MatchInfo matchInfo)
        {
            OnMatchCreate(success, extendedInfo, matchInfo);
            ShowLobby.Instance.OnMatchCreate();
        }



        // ----------- match game type ---------------
        public void FindMatchGame()
        {
            type = MatchType.team;
            Game2D.game.GameManager.localGame = false;
            if (!matchMaker)
                StartMatchMaker();
            ShowLobby.Instance.waitForFindIcon.gameObject.SetActive(true);
            matchMaker.ListMatches(0, 6, "", false, 0, 1, ShowMatchList);
        }

        void ShowMatchList(bool success, string extendedInfo, List<MatchInfoSnapshot> matches)  // only show MatchList
        {
            ShowLobby.Instance.waitForFindIcon.gameObject.SetActive(false);
            if (!success) return;
            if (type == MatchType.team)
                MatchMakerController.Instance.ShowMatches(matches);
            Debug.Log(" matches.Count " + matches?.Count);
        }
        public void CreateMath(string matchMane, int typeMatch, string password = null) // type 1 = match 
        {
            matchMaker.CreateMatch(matchMane, 4, true, password ?? "", "", "", 1, typeMatch, OnMatchCreateTeam);
            type = (MatchType)typeMatch;
        }
        public void OnGuiJoinMatch(NetworkID id, string password)
        {
            matchMaker.JoinMatch(id, password, "", "", 0, 1, OnMatchJoined);
        }



        public override void OnMatchJoined(bool success, string extendedInfo, MatchInfo matchInfo)
        {
            Debug.Log("Match Join  " + type.ToString()); //0 - fast game 1  match game  
            if (type == MatchType.fast)
            {
                ShowLobby.Instance.OnMatchJoin();
            }
            else
            {
                MatchMakerController.Instance.OnMatchJoin(success);
            }
            base.OnMatchJoined(success, extendedInfo, matchInfo);

        }

        public void RemovePlayer()
        {
            var player = lobbySlots.FirstOrDefault(p => p.isLocalPlayer);
            if (player)
                ClientScene.RemovePlayer(player.playerControllerId);
        }

        void OnMatchCreateTeam(bool success, string extendedInfo, MatchInfo matchInfo)
        {
            OnMatchCreate(success, extendedInfo, matchInfo);
            MatchMakerController.Instance.OnMatchCreate(matchInfo);
        }

        public override void OnMatchCreate(bool success, string extendedInfo, MatchInfo matchInfo)
        {
            base.OnMatchCreate(success, extendedInfo, matchInfo);
            currentMatchInfo = matchInfo;
        }

        /* IEnumerator WaitPlayerCoroutine(float timer)
         {
             yield return new WaitForSeconds(timer);
             ShowLobby.Instance.waitForConnect?.SetActive(true);
             waitMassage = true; 


         }*/
        public void OnClickStopMatchMaker()
        {
            StopMatchMaker();
            StopHost();
            ShowLobby.Instance.StartLobby(false);
        }


        public void OnGUIExitToLobby()
        {

            foreach (var player in lobbySlots)
            {
                if (player != null)
                {
                    var playerLobby = player as PlayerLobby;
                    if (playerLobby)
                    {
                        playerLobby.CmdExitToLobby();
                    }
                }
            }
        }

        public void DestoyMatch(NetworkID id, int domain)
        {
            if (id != NetworkID.Invalid)
                matchMaker.DestroyMatch(id, domain, OnDestroyMatch);
            //StopHost(); 
        }
        public override void OnDestroyMatch(bool success, string info)
        {
            base.OnDestroyMatch(success, info);

        }

        //-------------server--------------
        public override void OnLobbyStopHost()
        {
            // base.OnLobbyStopHost();
            Debug.Log("OnLobbyStopHost", this);


        }
        public override void OnLobbyStartHost()
        {
            // base.OnLobbyStartHost();
            Debug.Log("OnLobbyStartHost", this);
        }

        public override bool OnLobbyServerSceneLoadedForPlayer(GameObject lobbyPlayer, GameObject gamePlayer)
        {


            PlayerLobby lobby = lobbyPlayer.GetComponent<PlayerLobby>();
            game.NetPlayer player = gamePlayer.GetComponent<game.NetPlayer>();

            //player.playerGameName = lobby.playerName;
            //player.id = lobby.playerControllerId;
            //player.plId = lobby.netId;
            //player.index = Array.IndexOf(lobbySlots, lobby);
            Debug.Log(  "INDEX = " + player.index);
          //  player.CmdSendStartParam();
            Debug.Log(" OnLobbyServerSceneLoadedForPlayer" + lobby.playerName + "  " + lobby.netId);   // вызывается для каждого игрока но только на сервере        
            //  передать данные в игру из лобби
            return base.OnLobbyServerSceneLoadedForPlayer(lobbyPlayer, gamePlayer);
        }


        // ---    Client --- 

        public override void OnLobbyClientConnect(NetworkConnection conn)
        {
            Debug.Log("  OnLobbyClientConnect");
            if (type == MatchType.fast)
                ShowLobby.Instance.cancelButton.gameObject.SetActive(true);
            base.OnLobbyClientConnect(conn);
        }

        public override void OnClientError(NetworkConnection conn, int errorCode)
        {
            Debug.Log("  OnClientError " + errorCode, this);
            ShowLobby.Instance.waitForConnect?.SetActive(false);
            StopHost();

        }
        public override void OnLobbyClientDisconnect(NetworkConnection conn)
        {
            Debug.Log("OnLobbyClientDisconnect", this);
            ShowLobby.Instance.StartLobby(false);
            StopHost();

        }
        public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId)
        {

            base.OnServerAddPlayer(conn, playerControllerId); 
            ControlCount();           
        }

        void ControlCount()
        {
            if (currentMatchInfo == null) { Debug.LogError(" ON  server add/remove can't find match info"); return;  }
            var players = lobbySlots.Where(p => p).ToList();
            if (type == MatchType.fast) // fast game 
            {
                ShowLobby.Instance.UpdateMatchCount(players.Count);

            }
            if (type == MatchType.team)
            {
                Debug.Log(" PLayer Add At match");
                MatchMakerController.Instance.UpdateMatchCount(players.Count);
            }

        }
        public override void OnServerDisconnect(NetworkConnection conn)
        {
            base.OnServerDisconnect(conn);
            ControlCount();
            Debug.Log("OnServerDisconnect");
        }
        public override void OnServerRemovePlayer(NetworkConnection conn, PlayerController player)
        {

            Debug.Log("Remove  Player " + player.playerControllerId);
            if (lobbySlots.Where(p => p).Count() <= 1) ShowLobby.Instance.startButton.gameObject.SetActive(false);
            base.OnServerRemovePlayer(conn, player);
        }

        public override void OnLobbyStartClient(NetworkClient lobbyClient)
        {
            Debug.Log("OnLobbyStartClient");
            // before  connect call back 
        }

        public override void OnLobbyClientEnter()
        {
            Debug.Log("OnLobbyClientEnter");
            base.OnLobbyClientEnter();

        }
        public void SendReadyHostPlayer()
        {
            lobbySlots.FirstOrDefault(p => p.isLocalPlayer)?.SendReadyToBeginMessage();
        }
        public override void OnLobbyClientExit()
        {
            Debug.Log("OnLobbyClientExit");
            base.OnLobbyClientExit();

            if (SceneManager.GetActiveScene().name == base.playScene) //  переход на следующую сцену 
            {

                List<PlayerLobby> lobbyPlayer = lobbySlots.Where(p => p).Cast<PlayerLobby>().ToList();

                lobbyPlayer.ForEach(k =>
                {
                    k.SetContexIsLobby(false);
                    k.SetAllParam();
                    if (k.isLocalPlayer)
                    {
                        k.CmdAllAsync();
                    }
                });
            }
            else

            {
                Debug.Log("CLIENT EXIT"); 
            }
        }

        private void OnDestroy()
        {
            if (currentMatchInfo != null && matchMaker != null)
            {
                StopHost();
            }
        }

        public void QuitApp()
        {
            Application.Quit();
        }



        public void CloseMatch()
        {
            if (currentMatchInfo != null && matchMaker != null)
            {
                Debug.Log("CLOSE MATCH");
                ClientScene.ClearSpawners();
                ClientScene.DestroyAllClientObjects();
                matchMaker.DropConnection(currentMatchInfo.networkId, currentMatchInfo.nodeId, currentMatchInfo.domain, OnDropConectionMatch);
            }
        }

        void OnDropConectionMatch(bool success, string extendedInfo)
        {
            Debug.Log("<color=green>Match drop res = " + success + "  " + extendedInfo + " </color>");
            // StopMatchMaker();          
            StopHost();
            // todo  : open Match auto
        }

    }
}
