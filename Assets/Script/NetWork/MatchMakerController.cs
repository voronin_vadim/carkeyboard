﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking.Match;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.Networking.Types;
using System.Linq;
using System; 

namespace Game2D.Net
{
    public class MatchMakerController : MonoBehaviour
    {
        enum MatchState
        {
             not, 
             create ,
             waitPlayer , 
             waitStart ,
             waitServer
        }

        public static MatchMakerController Instance;
        [Space(20)]
        [Header("Matches infos")]
        public ShowMatch matchPrefab;
        int matchCount = 6;

        List<ShowMatch> showMatches;
       

        //  --- UI ---
        public Button updataMatches;     
        public Button sharePassword; 

      

        public GameObject passwordWindows;
        public List<GameObject>  createGameObject;
        public List<GameObject> joinGameobject;
        public InputField input;

        [Header("PasswordWindows")]
        public Button createMatch;
        public Button joinMatch;


        [Header("Match Buttons")]
        public Button startMatch; //  host   send event for  ready
        public Button destroyMatch;  //  destroy if create
        public Button openCreateMatch;
        public Button leaveMatch; 

        public Text stateText;

        //  text state 
        const string WAIT_CREATE = "waitForCreateServer"; 
        const string WAIT_SERVER = "waitServer";
        const string WAIT_PLAYER = "findContinue"; 


        string matchN; 
        string password;
        NetworkID hostNetworkId;
        LobbyManager loby; 

        private void Start()
        {
            InitMatches();
            Instance = this;
        }

        void SetMatchButtonState(MatchState state)
        {
            destroyMatch.gameObject.SetActive(false);
            openCreateMatch.gameObject.SetActive(false);
            startMatch.gameObject.SetActive(false);
            leaveMatch.gameObject.SetActive(false);
            stateText.text = "";

            switch (state)
                  {
                case MatchState.not:
                    {
                        openCreateMatch.gameObject.SetActive(true);
                       
                    }
                    break;
                case MatchState.create:
                    {
                        stateText.text = LocalizationService.Instance.GetTextByKey(WAIT_CREATE);
                        
                    }
                    break;
                case MatchState.waitPlayer:
                    {
                        destroyMatch.gameObject.SetActive(true);
                        sharePassword?.gameObject.SetActive(password != "");
                        stateText.text = LocalizationService.Instance.GetTextByKey(WAIT_PLAYER);
                        
                    }
                    break;
                case MatchState.waitStart:
                    {
                        destroyMatch.gameObject.SetActive(true);
                        startMatch.gameObject.SetActive(true); 
                        sharePassword?.gameObject.SetActive(password != "");
                       
                    }
                    break;
                case MatchState.waitServer:
                    {
                        leaveMatch.gameObject.SetActive(true);
                        stateText.text = LocalizationService.Instance.GetTextByKey(WAIT_SERVER);
                    }
                    break; 

            }
        }

        public void UpdateMatchCount(int players)
        {
            //  Debug.LogError("Do not have host match");  
            bool canStart = players >= 2;
            SetMatchButtonState(canStart ? MatchState.waitStart : MatchState.waitPlayer); 
        }

        public void OnMatchJoin(bool success)
        {
            SetMatchButtonState((success) ? MatchState.waitServer : MatchState.not);
            //to do  включить состояние ожидание сервера 
        }

       public void OnMatchCreate(MatchInfo info)
        {
            hostNetworkId = info.networkId;   
            showMatches.ForEach(p => p.SetEnable(hostNetworkId));
            SetMatchButtonState(MatchState.waitPlayer); 
        }

        public void ShowMatches(List<MatchInfoSnapshot> matches)
        {
            if (Game2D.UI.Windows.Curren.Name != UI.NavigationButtonType.Match)
                Game2D.UI.Windows.ShowWindows(UI.NavigationButtonType.Match);

            ShowMatch match; // temp 
            matches = matches.Take(matchCount).ToList();  //  cut for max 
            showMatches.ForEach(p => p.gameObject.SetActive(false));
            int count = matches.Count;
            for (int i = 0; i < count; i++)
            {
                match = showMatches[i];
                match.Show(matches[i], i);
                match.SetEnable(hostNetworkId);
                // match Action 
                match.onJoinMatch = SelectMatch; 
            }
        }
        void SelectMatch(NetworkID id , bool withPassword)
        {
            if (!withPassword)
                loby.OnGuiJoinMatch(id, "");
            else
            {
                OpenPasswordWindows(false);
                joinMatch.onClick.RemoveAllListeners();  
                joinMatch.onClick.AddListener(() => JoinGame(id));
            }

        }


      

        void OpenPasswordWindows( bool isSet) // get or set password
        {
            passwordWindows.SetActive(true);
            input.text = "";
            createGameObject.ForEach(p => p.SetActive(isSet));
            joinGameobject.ForEach(p => p.SetActive(!isSet));
        }
        void EnterPassword()
        {
            passwordWindows.SetActive(false);
            password = input?.text ?? "";
        }
         void CreateGame() // on Button create match 
        {
            EnterPassword();
             matchN = PlayerPrefs.GetString(Config.ConfigManager.NICK_NAME);
            string isFree = String.IsNullOrEmpty(password) ? "free" : "";
            loby.CreateMath(matchN + isFree ,  1, password);
            loby.FindMatchGame();
        }
        void JoinGame( NetworkID id )
        {
            // to do   check have match 
            EnterPassword();
            loby.OnGuiJoinMatch(id, password);
        }

        void SharePassword()
        {         
                string gamePassword ="Password = " +password;
                //ButtonShare.Share(gamePassword, "");
            new NativeShare().SetText(gamePassword).Share();
        }



        void InitMatches()
        {
            loby = LobbyManager.l_Singleton; 
            if (matchPrefab == null)
            {
                Debug.LogError("Do not set match prefab");
                return;
            }
            SetMatchButtonState(MatchState.not);
            sharePassword.gameObject.SetActive(false); 
            //  init   action Match button
            createMatch?.onClick.AddListener(() => CreateGame());
            sharePassword?.onClick.AddListener(() => SharePassword()); 
            updataMatches?.onClick.AddListener(() => { loby.FindMatchGame(); });
            openCreateMatch?.onClick.AddListener(() =>  {OpenPasswordWindows(true); });
            startMatch?.onClick.AddListener(() => loby.SendReadyHostPlayer());
            destroyMatch?.onClick.AddListener(() =>
            {
                loby.CloseMatch();
                SetMatchButtonState(MatchState.not); 
            });
            leaveMatch?.onClick.AddListener(() =>
            {
                var pl = loby.lobbySlots.Where(p => p).FirstOrDefault(k => k.isLocalPlayer);
                ClientScene.RemovePlayer(pl.playerControllerId);
                Net.LobbyManager.l_Singleton?.StopClient();  // for test in game scene
            }
            ); 
            showMatches = new List<ShowMatch>(matchCount);
            for (int i = 0; i < matchCount; i++)
            {
                ShowMatch newMatch;
                Transform parent = matchPrefab.transform.parent;
                if (i == 0)
                {
                    newMatch = matchPrefab;
                }
                else
                {
                    newMatch = Instantiate(matchPrefab, parent);
                    newMatch.transform.localScale = Vector3.one;
                }
                showMatches.Add(newMatch);
            }
            showMatches.ForEach(p => p.gameObject.SetActive(false));
        }
    }
}
