﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System;

public class AdRewardButton : MonoBehaviour

{
    Button btn;

    public string rewardType = "coin";
    public int rewardValue = 0;


    [Header("Disable Timer")]
    public float disableTime = 3f;
    public float disableTimeAfterShow = 0f;
    float startingTimeDisable; 
    

    [Header("Event")]
    public UnityEvent onShow; // add Event  for example Hide after Show
    public Action<AdRewardButton> OnClick = delegate { };

    Coroutine disableCoroutineAfterCkick;
    Coroutine disableCoroutineAfterShow;
    private void OnEnable()
    {
        btn.interactable = true;
        if (startingTimeDisable > 0)
        {
            disableCoroutineAfterShow = StartCoroutine(DisableCoroutine(startingTimeDisable, () => btn.gameObject.SetActive(true), disableCoroutineAfterShow));
            startingTimeDisable = Time.time + startingTimeDisable; // addCurrent Time  ; 
        }
        else btn.gameObject.SetActive(true); 

    }

    private void Awake()
    {

        btn = GetComponentInChildren<Button>();
        onShow.AddListener(OnShow); //  strange

    }

    
    private void OnDisable()
    {
        if (disableCoroutineAfterCkick != null)
        {
            StopCoroutine(disableCoroutineAfterCkick); 
            disableCoroutineAfterCkick = null;
            btn.interactable = true;
        }
        if (disableCoroutineAfterShow != null)
        {
            StopCoroutine(disableCoroutineAfterShow);
            disableCoroutineAfterShow = null; 
            startingTimeDisable -= Time.time;
            if (startingTimeDisable < 0)
            {
                Debug.LogError("Timer on Disable button less then in coroutine.  Maybe corounine do not stop");
                startingTimeDisable = 0; 
            }
        }
        else
        startingTimeDisable = 0; 
    }
    void OnShow()
    {
        if (disableTimeAfterShow <= float.Epsilon) return;
        disableCoroutineAfterShow = StartCoroutine(DisableCoroutine(disableTimeAfterShow, () => btn.gameObject.SetActive(true), disableCoroutineAfterShow));
        btn.gameObject.SetActive(false);


      //  Debug.Log("REWWAARRRDD  SHOOWW"); 
        //show Button After timer 
      
        startingTimeDisable = Time.time + disableTimeAfterShow;
    }
    void Start()
    {
        btn.onClick.RemoveAllListeners();
        btn.onClick.AddListener(SimpleClick);
       
    }
    public void SimpleClick()
    {
        OnClickADD();
        AdShowManager.Instance.RewardForButton(this);
        
    }
    private void OnDestroy()
    {
        btn.onClick.RemoveAllListeners();
    }
    void OnClickADD()
    {
        btn.interactable = false;
        disableCoroutineAfterCkick = StartCoroutine(DisableCoroutine(disableTime, () => btn.interactable = true, disableCoroutineAfterCkick));
        OnClick.Invoke(this);

    }
    IEnumerator DisableCoroutine(float time, Action action, Coroutine cor)
    {
        yield return new WaitForSecondsRealtime(time);
        action.Invoke();
        cor = null;

    }
}

