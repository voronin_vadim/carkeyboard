﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;


public class AdShowManager : MonoBehaviour

{
  

    static AdShowManager instance;
    public static AdShowManager Instance
    {
        get {

            if (instance == null)
            {
                instance = FindObjectOfType<AdShowManager>(); 
            }
            return instance; 
        }
    }

    bool adsEnable = true;

    public bool bannerOnStart;
   
    //bool state
    static bool openAppFirst =false;
    bool unFocusByAds;
  bool internalComplete = false; 
   public  bool ununFocusByManyCauses; 


    public AdManager adManager;
    bool testBunner; 
    float timer;
    float currentTimeScale;
    float audioLisenerValue;

    [System.Serializable]
    public class InternalTypeAD
    {
        public string name;
        public int numberToAction;
        public int current;
        public float delay;

        public InternalTypeAD(string _name, int _numberToAction, float _delay)

        {
            name = _name;
            numberToAction = _numberToAction;
            delay = _delay; 
        }
    }

    public void ShowBunner(bool show)
    {

        testBunner = false;
#if UNITY_EDITOR
        testBunner = show;
#else    
        if (show && adsEnable)
            // onShowBanner();
            adManager.ShowBanner(); 

        else
            //onCloseBanner();
        adManager.DestroyBanner();
#endif

    }

     public void BuyDisableAds()
    {
       PlayerPrefs.SetInt("AdsDisable", 1);
       ShowBunner(false);
        adsEnable = false; 
    }
    public void CompleteInitAd()
    {
        adsEnable = PlayerPrefs.GetInt("AdsDisable", 0) ==0;
#if NO_ADS_VERION
        adsEnable = false;
#endif

        if (bannerOnStart && adsEnable)
            //onShowBanner.Invoke();
            adManager.ShowBanner();
        else
            //onCloseBanner.Invoke();
            adManager.DestroyBanner();
    }

    private void OnGUI()
    {
        if (!testBunner) return;
        return; 
        GUIStyle style = new GUIStyle();
        Rect testAdsBanner = new Rect(0,Screen.height-50, 355, 50);
        style.alignment = TextAnchor.MiddleCenter;
        style.fontSize = (int)(Screen.height * 0.06);
        style.normal.textColor = new Color(0.0f, 0.0f, 0.5f, 1.0f);  
        GUI.Box(testAdsBanner, "TEST BUNNER");
        
    }

    public static List<InternalTypeAD> adAllType;


    void InitInternaType()
    {
        if (adAllType == null || adAllType.Count == 0)
        {
            //setting into scene
            //setDefalt Internal AD
            adAllType = new List<InternalTypeAD>();
            adAllType.Add(new InternalTypeAD("menu", 2, 0f)); 
            adAllType.Add(new InternalTypeAD("shop", 5, 1f)); 
            adAllType.Add(new InternalTypeAD("lose", 7, 1f)); 
            adAllType.Add(new InternalTypeAD("win", 1, 0f)); 
            adAllType.Add(new InternalTypeAD("map", 11, 0f)); 
            adAllType.Add(new InternalTypeAD("pause", 2, 2f));
        }

        
    }


    AdRewardButton curButton;
    public void CheckButton(AdRewardButton button)
    {
      //  button.OnClick += RewardForButton;
    }
    public void Show(string type)
    {
        if (!adsEnable)
        {
            Debug.Log("Ads unable");
            return;
        } 
         InternalTypeAD ad = adAllType.FirstOrDefault(p => p.name == type);
        if (ad == null)
        {
            Debug.Log($"NOt found advertisnment with it name {type} ");
            return; 
        }
        ad.current++;
     //   Debug.Log($"ADS event type  {ad.name } , current  { ad.current} , need {ad.numberToAction}");
        if (ad.numberToAction <= ad.current)
        {
            ad.current = 0;
#if UNITY_EDITOR
            Debug.Log($"Show ads type {ad.name } , with delay { ad.delay}");
            return;
#else
            if (adManager == null)
            {
                Debug.LogError("Ad Manager do not set"); 
            }
            if (adManager.InternalLoaded())
            {
                unFocusByAds = true; 
                StartCoroutine(ShowInternalCoroutine(ad.delay));
            }
#endif
        }

    }

    public void GetReward()
    {
        //onCompleteReward.Invoke(); 
        curButton.onShow.Invoke();

        if (curButton.rewardType == "coin")
        {
            Game2D.Shop.ShopController.AddMoney(curButton.rewardValue);
        }
       

    }
   public void RewardForButton(AdRewardButton button)
    {
        curButton = button;
#if UNITY_EDITOR
        GetReward();
#else
         unFocusByAds = true; 
        adManager.ShowRewardBasedVideo(); 
#endif
    }
    private void Start()
    {
        InitInternaType();
        openAppFirst = true;


    }
   private void Update()
    {
        //InternalComplete  calling  not in thr main Thread
        if (internalComplete)
        {
            InternalCompleteMainTread();
            internalComplete = false; 
        }
    }
    private void Awake()
    {
         
        adsEnable = PlayerPrefs.GetInt("AdsDisable", 0) == 0;
    }
     void InternalCompleteMainTread()
    {



        Time.timeScale = currentTimeScale;
        AudioListener.volume = audioLisenerValue;
        Debug.Log("Internal show complete");
    }
    public void InternalComplete()
    {
        internalComplete = true;


    }

    void OnApplicationFocus(bool hasFocus)
    {

        
        if (!hasFocus) return;
        
        if (openAppFirst && !unFocusByAds && !ununFocusByManyCauses)
        {
         //   Instance.Show("game");
        }
        unFocusByAds = false;
        ununFocusByManyCauses = false;

    }




    IEnumerator ShowInternalCoroutine(float dalay)
    {
        yield return new WaitForSecondsRealtime(dalay);
        currentTimeScale = Time.timeScale;
        Time.timeScale = 0.0f;
        audioLisenerValue = AudioListener.volume; 
        AudioListener.volume = 0.0f;    
        adManager.ShowInternal(); 
    }
}
