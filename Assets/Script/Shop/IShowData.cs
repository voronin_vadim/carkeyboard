﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Linq;

namespace Game2D.Shop {
    //[RequireComponent(typeof(Button))]
    public class IShowData : MonoBehaviour {

        Action<IShowData> onAction = delegate { };
        public Graphic targerGrafic;
       
        [HideInInspector]
        public GameObject addingElement;
        public Text costText;  // cost
        public Text discription; 
        public GameObject levelGO;
        


        //Add select button
        public Transform addElementTransform;

        public void AddInteractivElement(GameObject go)
        {
            addingElement = go;
            if (addElementTransform != null)
                go.transform.parent = addElementTransform;
            else go.transform.parent = transform;
            go.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;
            go.transform.localScale = Vector3.one; 
        }
        
        private void Awake()
        {
         //  if ( levelGO)levelGO.SetActive(false); 
        }
        public void AddListenerToObj(Action<IShowData> action)
        {
            onAction += action;
        }
        public void Start()
        {
            GetComponentInChildren<Button>().onClick.AddListener(() => Action());
        }
        public virtual void Action()
        {
            onAction.Invoke(this);
        }

    } }
