﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;


namespace Game2D.Shop {
    public class SelectorButtons : MonoBehaviour
    {

        [SerializeField]
        UnityEvent onAccept;
        [SerializeField]
        UnityEvent onRefuse;

        [SerializeField]
        Button acceptButton;

        [SerializeField]
        Button refuseButton;
        // Use this for initialization
        void Awake()
        {

            acceptButton.onClick.AddListener(() => onAccept?.Invoke());
            refuseButton.onClick.AddListener(() => onRefuse?.Invoke());

        }
        public void AddOnAccept(UnityAction act)
        {
            onAccept.AddListener(act);
        }
        public void AddOnRefuse(UnityAction act)
        {
            onRefuse.AddListener(act);
        }
       

        private void OnDisable()
        {
            gameObject.SetActive(false);
           // ClearEvent();
        }

        public void ClearEvent()
        {
            onAccept.RemoveAllListeners();
            onRefuse.RemoveAllListeners();
        }
        // Update is called once per frame
        void Update()
        {

        }
    }
}
