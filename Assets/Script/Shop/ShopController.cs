﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Linq; 

namespace Game2D.Shop {


  


    public class ShopController : MonoSingleton<ShopController> {

        public Text coinText; 
        static int coin;
        public static  int Coin => coin;
        public IShowData selectedElement;
        public int selectedIndex; 
        public ShopTabBase selectedTab; 

        public SelectorButtons selectorGameObject;//PREFAB
        public SelectorButtons selectorGameObjectCurrent; //  ON SCENE


        [Header("Shop Tabs")]
        public GameObject coinTab; 


        public List<ShopTabBase> shopTabs; 


        const string COIN_VALUE = "GameCoin";
        const string EXP_VALUE = "ExpAmount";
        const string LVL = "LVL"; 

        // publicb List<IShowData>


        // level
        [Header("Level Progress")]
        public List<Text> levelText;
        public List<Image> progressFill;

        [Header("Level Exp Values")]
        public List<int> levelValue;

        static int  currentExpa; 
        static int currentLevel;
        public static  int CurrentLevel => currentLevel;
        Action<int> OnLevelChange = delegate { };
      static  Action OnExpaChange = delegate { }; 
      static  Action OnMoneyChange = delegate { };


        public void SelectElement()
        {
            if (selectedElement != null)
            {
                Debug.Log("Select Element" + selectedElement.name);
            }
            else
            {
                
                Debug.Log("Do not select elemrnt"); 
            }


            Debug.Log("Selected index = " + selectedIndex);
            selectedTab.ApplyShopElem(selectedIndex);
            CancelSelect(); 
        }

        public void CancelSelect()
        {
            selectedElement = null;
            selectorGameObjectCurrent.gameObject.SetActive(false); 
        }

        public void InitData()
        {
            coin = PlayerPrefs.GetInt(COIN_VALUE, 0);
            currentExpa = PlayerPrefs.GetInt(EXP_VALUE, 0);
            currentLevel = PlayerPrefs.GetInt(LVL, 0);
            ShowLevel(); 
        }

      static void Save()
        {
            PlayerPrefs.SetInt(EXP_VALUE, currentExpa);
            PlayerPrefs.SetInt(COIN_VALUE, coin);
            PlayerPrefs.Save(); 
        }

        public void AddLisenerToCheckLevel(Action<int> action)
        {
            OnLevelChange += action;
            action(currentLevel); //  invoke
        }
        public void RemoveLisenerToCheckLevel(Action<int> action)
        {
            OnLevelChange -= action;
            
        }


        public void OpenShop(int index)
        {
            CloseAllTab();

            if (index < shopTabs.Count)
            {
                shopTabs[index].gameObject.SetActive(true);
            }
            else coinTab?.SetActive(true);
            UI.Windows.ShowWindows(UI.NavigationButtonType.ShopTab); 

        }
        void CloseAllTab()
        {
            coinTab?.SetActive(false);
            shopTabs.ForEach(p => p.gameObject.SetActive(false)); 
        }
        public void OnClickData(int index ,IShowData data ,  ShopTabBase tab)
        {
            selectedIndex = index; 
            selectedElement = data;
            data.AddInteractivElement(selectorGameObjectCurrent.gameObject);
            selectorGameObjectCurrent.gameObject.SetActive(true);
            selectedTab = tab; 
        }

        public bool Buy(int cost)
        {
            if (cost > coin) return false;
            coin -= cost;
            Save();
            OnMoneyChange();
            SoundManager.PlaySound("buy");
            return true;

        }

        // Use this for initialization
        void Start() {
            InitData();
            ShowLevel();
            ShowCoin(); 
            OnExpaChange += ShowLevel;
            OnMoneyChange += ShowCoin; 


            selectorGameObjectCurrent = Instantiate(selectorGameObject);
            selectorGameObjectCurrent.AddOnRefuse(CancelSelect);
            selectorGameObjectCurrent.AddOnAccept(SelectElement);
            
            shopTabs.ForEach(p => p.InitShop()); 
        }
        private void OnDestroy()
        {
            OnExpaChange -= ShowLevel;
            OnMoneyChange -= ShowCoin; 
        }

        public static void AddMoney(int value)
        {
            coin += value;
            OnMoneyChange(); 
            Save(); 
        }
        public static void AddExpa(int value)
        {
            currentExpa += value;
            Save();
            OnExpaChange.Invoke();
        }
        void ShowCoin()
        {
            coinText.text = coin.ToString();
        }
        void ShowLevel()
        {
            int maxValue = levelValue.Last();
            if (currentExpa >= maxValue)
            {
                progressFill.ForEach(p => p.fillAmount = 1f);
                levelText.ForEach(p => p.text = "Max");
                return;
            }
            float levelProgress;
            int currentLevelTexp;
            currentLevelTexp = levelValue.FindIndex(0, p => p > currentExpa);

            //calculate progress
            int nextLeveExp = levelValue[currentLevelTexp]; 
            int prevLevelExp;
            if (currentLevelTexp == 0) prevLevelExp = 0;
            else prevLevelExp = levelValue[currentLevelTexp - 1];
            levelProgress = (currentExpa - prevLevelExp) / (float)(nextLeveExp - prevLevelExp);

            //Action On Change
            if (currentLevelTexp != currentLevel)
            {
                currentLevel = currentLevelTexp;
                Save(); 
                OnLevelChange(currentLevel);
            }

            //update UI
            progressFill.ForEach(p => p.fillAmount = levelProgress);
            levelText.ForEach(p => p.text = LVL + " " + currentLevelTexp.ToString());
        }
    }
}
