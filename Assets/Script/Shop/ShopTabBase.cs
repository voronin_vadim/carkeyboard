﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Text;
using System.Linq;
using UnityEngine.UI;


namespace Game2D.Shop
{

    public  class ShopTabBase : MonoBehaviour
    {
        [SerializeField]
        protected List<int> cost;
        [SerializeField]
        protected List<int> needLevel;

        bool init = false;
        public Config.ConfigType type; 

        // on Shop
        [SerializeField]  // test
        protected List<IShowData> showShop;
        public  IShowData shopDataPrefab; 

        // on Profile
        [SerializeField]     
        List<IShowData> showProfile;
       public  IShowData profileDataPrefab; 
      
        public  bool[] buyElement;

        void SaveData()
        {
            string saveString = String.Join("/", buyElement);
            PlayerPrefs.SetString("Shop" + type.ToString(), saveString);
            PlayerPrefs.Save();
        }

        public void InitShop()
        {
            if (init) return;

           
            if (!PlayerPrefs.HasKey("Shop" + type.ToString()))
            {
                buyElement = cost.Select(p => p == 0).ToArray(); 
                SaveData();
              
            }

            string[] saveStrings = PlayerPrefs.GetString("Shop" + type.ToString()).Split(new string[] { "/" }, StringSplitOptions.None);
            buyElement = saveStrings.Select(p => Convert.ToBoolean(p)).ToArray();
            //customizeShow element
            int elementCount = buyElement.Length;
            showShop = new List<IShowData>();
            showProfile = new List<IShowData>(); 

            for (int i = 0; i < elementCount; i++)
            {
                if (i == 0)
                {
                    showShop.Add(shopDataPrefab);
                    showProfile.Add(profileDataPrefab);
                }
                else
                {
                    showShop.Add(Instantiate(shopDataPrefab, shopDataPrefab.transform.parent));
                    showProfile.Add(Instantiate(profileDataPrefab, profileDataPrefab.transform.parent));
                } 
            }

            for (int i = 0; i < elementCount; i++)
            {
                showShop[i].AddListenerToObj(TryBuyItem);
                showProfile[i].AddListenerToObj(TrySelectItem);
                showShop[i].costText.text = cost[i].ToString(); 

                InitShowData(showShop[i], i);
                InitShowData(showProfile[i], i);
                UpdataStateShow(i);
            }
        }
      
        void UpdataStateShow(int i)
        {
           
            showShop[i].gameObject.SetActive(!buyElement[i]);
            showShop[i].levelGO.SetActive(needLevel[i] > 0 && needLevel[i] > ShopController.CurrentLevel);
            showShop[i].levelGO.GetComponentsInChildren<Text>().Last().text = needLevel[i].ToString(); 
            showProfile[i].gameObject.SetActive(buyElement[i]);
        }

        void TrySelectItem(IShowData data)
        {
            int index = showProfile.IndexOf(data);
            ShopController.Instance.OnClickData(index,data , this);          
        }
        void TryBuyItem(IShowData data)
        {
            int index = showShop.IndexOf(data);
            if (buyElement[index])
            {
                Debug.LogWarning("Try buy byued element");
                return;
            }


            if (ShopController.Instance.Buy(cost[index]))
            {
                OpenFree(index);
            }


        }

        public void OpenFree(int i) // also IAP
        {
            buyElement[i] = true;
            SaveData();
            UpdataStateShow(i);
            ApplyShopElem(i);
        }
        public virtual void ApplyShopElem(int index)
        {
            Config.ConfigManager.Instance.ChangeConfig(type, index);
        }

     
        
        protected virtual void InitShowData(IShowData show, int element)
        {

            switch (type)
            {
                case Config.ConfigType.borders:
                    {
                        var configData = Config.ConfigData.Instance.GetConfigType(type, element);
                        ((Image)show.targerGrafic).sprite = ((ConfigBorder)configData).mainSprite; // ((List<Sprite>)configData)[0];

                    }
                    break;

                case Config.ConfigType.cars:
                    {
                        var configData = Config.ConfigData.Instance.GetConfigType(type, element);
                        ConfigCar car = ((ConfigCar)configData); 
                        ((Image)show.targerGrafic).sprite = car.mainSprite;
                       if( show.discription) show.discription.text = car.carName;

                       /* ((Image)show.targerGrafic).sprite = (Sprite)configData[0];
                        if (show.discription) show.discription.text = (string)configData[1];*/
                    }
                    break;

                case Config.ConfigType.font:
                    {
                        var configData = Config.ConfigData.Instance.GetConfigType(type, element);
                        ((Text)show.targerGrafic).font = ((ConfigFont)configData).font;
                        ((Text)show.targerGrafic).text = "Examle Font " + element.ToString();
                    }
                    break;
            }
           
           
        }// diiferent implementation
    }
}
