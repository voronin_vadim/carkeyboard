﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;
using System.Linq;
     

namespace Game2D
{
    public class CheatHelp : MonoBehaviour
    {

        private void Awake()
        {

#if FINAL
        gameObject.SetActive(false); 
#endif
        }

        public void AddMoney()
        {
            Shop.ShopController.AddMoney(1000);
        }

        public void AddExpa()
        {
            Shop.ShopController.AddExpa(20); 
        }

        public void ClearPlayerPrefs()
        {
            PlayerPrefs.DeleteAll();
            SceneManager.LoadScene("main");
        }
        public void TestHome()
        {
            UI.WindowsManager.CloseCurrentWindowsStatic();
        }

        public void StopServer()
        {
            Game2D.Net.LobbyManager.l_Singleton.StopServer();
        }

        public void Leave()
        {       
            Network.Disconnect();
           
            NetworkServer.DisconnectAll();


           

             Destroy(Game2D.Net.LobbyManager.l_Singleton.gameObject);
        }

        public void RemovePlayer()
        {
            var player = Game2D.Net.LobbyManager.l_Singleton.lobbySlots.Where(l=>l).FirstOrDefault(p => p.isLocalPlayer);
            if (player)
                ClientScene.RemovePlayer(player.playerControllerId);
        }

        public void StopClient()
        {
            Game2D.Net.LobbyManager.l_Singleton.StopClient();
        }

        public void SceneMenu()
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("main"); 
        }

    }
}
