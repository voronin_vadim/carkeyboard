﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



 namespace Game2D.UI {

   // [RequireComponent(typeof(Image))]
    [RequireComponent(typeof(Button))]

    public class NavigationButton : MonoBehaviour {

        //type
        public NavigationButtonType type => _type;
        [SerializeField]
        private NavigationButtonType _type;

        //privat
        Button _targetButton;
        bool init;

        // Use this for initialization
        void Start() {
            SetupButton();
        }

        void SetupButton()
        {
            _targetButton = GetComponent<Button>();
            _targetButton.onClick.AddListener(OnClick);
        }
        void OnClick()
        {
            EventManager.TriggerAction(_type);
        }


    }
}
