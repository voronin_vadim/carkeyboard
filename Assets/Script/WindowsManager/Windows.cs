﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System.Linq;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;

namespace Game2D.UI { 

    
    public enum NavigationButtonType
    {
        Default = 0,
        CloseWindows = 1,
        Menu = 2,
        Shop = 3,
        Pause = 4,
        Map = 5,
        Achieve = 6,
        Upgrade = 7,
        Booster = 8,
        Main = 9,
        InApp = 10,
        NoMoney = 11,
        Setting = 12,
        Input = 13,
        ShopTab = 14,
        Win = 15  , 
        Lose = 16, 
        Match = 17 , 
        Iap = 18 , 
    }

    public enum WindowsTypePopup
    {
        win, popup
    }

    [RequireComponent(typeof(CanvasGroup))]
    public class Windows : MonoBehaviour
    {
        static bool isAnimated;
        static Windows animatedWindows;


        public WindowsTypePopup type;
        // public string winName;

        public RectTransform item;

        [SerializeField]
        NavigationButtonType winName;

        public NavigationButtonType Name => winName;
        public static Action<NavigationButtonType> OnChangeActiveWindows = delegate { };

        [Header("FadeParam")]
        public float fadeIn = 0.5f;
        public float fadeOut = 0.5f;
        public bool scaling = false;
        public Vector3 scaleVector = new Vector3(0.7f, 0.7f, 0.7f);
        public bool moveing = false;
        public Vector2 moveVector = new Vector3(50f, 50f);

        //Action
        public UnityEvent onShow;
        public UnityEvent onHide;

        //init param
        CanvasGroup _canvasG;
        Vector2 _startPos;
        bool _inited;

        //windows graph
        [SerializeField]
         Windows prev;
        [SerializeField]
        static Windows curr;

        public static Windows Curren => curr; 
        //find All
        private static List<Windows> _all = null;
        public static List<Windows> All
        {
            get
            {
                if (_all == null||_all.Count==0)
                {
                    _all = Resources.FindObjectsOfTypeAll<Windows>().ToList();
                    SceneManager.sceneLoaded += Clear;

                }
                return _all;
            }
        }
        static void Clear(Scene _, LoadSceneMode __)
        {
            if (_all != null)
                _all.Clear();
            curr = null;
            OnChangeActiveWindows(NavigationButtonType.Default);
            SceneManager.sceneLoaded -= Clear;

        }

        void Awake()
        {
            Initialize();
        }

        public static void CloseCurentWindows()
        {
            if (curr)

                curr.CloseWindows();
        }
        public void CloseWindows()
        {

            if (isAnimated)
            {
                Debug.Log("Animated right Now");
                return;
            }

            if (!gameObject.activeInHierarchy) { Debug.LogError("Trying close not active windows"); return; }
            /* if (type == WindowsTypePopup.win)
             { if (prev)
                 {
                     Hide();
                     prev.Show(); 
                     curr = prev;
                     prev = null;
                 }
             }
             else
             {
                 Hide();
                 curr = prev;
                 prev = null;
             }*/
            if (type == WindowsTypePopup.popup || prev)
            {
                Hide();
                prev?.Show();
                curr = prev;
                OnChangeActiveWindows(curr.Name);
                prev = null;
            }
        }


        /// <summary>
        /// HideAllWindows
        /// </summary>
        public static void HideAll()
        {
            All.ForEach(p => p.gameObject.SetActive(false));
        }

        /// <summary>
        /// ShowWindows by enum
        /// </summary>
        public static Windows ShowWindows(Enum en, Action onShowAction = null)
        {
            string wind = en.ToString();
            return ShowWindows(wind, onShowAction);
        }

        /// <summary>
        /// ShowWindows for Button andAction
        /// </summary>
        public static Windows ShowWindows(string winName, Action onShowAction = null)
        {
            Windows win = All.Where(p => p.winName.ToString() == winName).FirstOrDefault();
            if (!win) return null;
            if (curr == win) return curr;



            if (win.type == WindowsTypePopup.popup)
            {
                win.prev = curr;
            }
            else if (win.type == WindowsTypePopup.win)
            {
                if (curr) win.prev = HideTree(curr);
            }

            var sq = DOTween.Sequence();
            sq.Append(win.Show());
          //  sq.AppendCallback(() => onShowAction());
            curr = win;
            OnChangeActiveWindows(curr.Name);
            curr.onShow.Invoke();
            return win;
        }
        /// <summary>
        /// Hide  stack Open Wind
        /// </summary>
        /// <param name="wind">windParam</param>
        static Windows HideTree(Windows wind)
        {
            if (wind.type == WindowsTypePopup.win)
            {
                wind.Hide();
                //wind.prev = null;
                return wind;
            }

            else if (wind.type == WindowsTypePopup.popup)
            {
                wind.Hide(0f);
                // wind.prev = null;
                if (wind.prev) return HideTree(wind.prev);

            }
            return null;

        }


        Tween Hide(float? overrideTime = null)
        {
            Initialize();
#if UNITY_EDITOR
            var s = "<color=red>Hide</color>: ";
            s += "<color=cyan>" + winName + "</color>|";
            Debug.Log(s);
#endif
            onHide.Invoke();
            float timeAnim = overrideTime ?? fadeOut;
            if (timeAnim <= float.Epsilon)
            {
                _canvasG.alpha = 0;
                gameObject.SetActive(false);
                return null;
            }
            _canvasG.interactable = false;


            var sq = DOTween.Sequence();
            sq.Append(_canvasG.DOFade(0f, fadeOut));
            if (scaling)
                sq.Join(item.DOScale(scaleVector, fadeOut)).SetUpdate(true);
            if (moveing)
                sq.Join(item.DOAnchorPos(_startPos + moveVector, fadeOut)).SetUpdate(true);
            sq.AppendCallback(() => gameObject.SetActive(false));
            return sq;

        }

        Tween Show(float? overrideTime = null)
        {

            isAnimated = true;
            Initialize();
            gameObject.SetActive(true);

            float animTime = overrideTime ?? fadeIn;
            if (animTime <= float.Epsilon)
            {
                _canvasG.alpha = 1;
                return null;
            }
            _canvasG.alpha = 0;
            var sq = DOTween.Sequence();
            sq.AppendCallback(() => gameObject.SetActive(true));

            sq.Append(_canvasG.DOFade(1f, fadeIn));
            if (scaling)
            {
                sq.Insert(0, item.DOScale(scaleVector, 0f)).SetUpdate(true);
                sq.Join(item.DOScale(Vector3.one, fadeIn)).SetUpdate(true);
            }
            if (moveing)
            {
                item.anchoredPosition = (_startPos + moveVector);
                sq.Join(item.DOAnchorPos(_startPos, fadeIn)).SetUpdate(true);
            }
            sq.AppendCallback(() => { _canvasG.interactable = true; isAnimated = false; });

            return sq;
        }


        void Initialize()
        {
            if (_inited) return;

            _canvasG = GetComponent<CanvasGroup>();
            _startPos = item.anchoredPosition;
            _canvasG.interactable = false;
            _inited = true;
        }
    }
}
