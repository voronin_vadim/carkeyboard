﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;


namespace Game2D.UI {
    public class WindowsManager : MonoBehaviour
    {
        public static Action onNavigationButtonClick = delegate { };

        public NavigationButtonType startWindows; 
        public void Start()
        {
            Windows.HideAll();
            if (startWindows!= NavigationButtonType.Default )
            Windows.ShowWindows(startWindows);
            
            InitAction();

        }

        void OnGUI()
        {
            if (Input.GetKeyUp(KeyCode.Home) || Input.GetKeyDown(KeyCode.Escape))
            {
                CloseCurrentWindows();
            }
        }


        public void InitAction()
        {

            EventManager.AddListener(NavigationButtonType.CloseWindows, CloseCurrentWindows);
            foreach (string winName in Enum.GetNames(typeof(NavigationButtonType)))
            {
                if (winName != NavigationButtonType.CloseWindows.ToString())
                    EventManager.AddListener(winName, () => { Windows.ShowWindows(winName); onNavigationButtonClick.Invoke(); });
            }


        }

        
        public void CloseCurrentWindows()
        {
            Windows.CloseCurentWindows();
        }
        public static void CloseCurrentWindowsStatic()
        {
            Windows.CloseCurentWindows();
        }


        public void OpenWindows(string windows)
        {
            Windows.ShowWindows(windows);
        }

        public void Pause(bool open)
        {
            if (open)
                Windows.ShowWindows("pause", onShowAction: () => { Debug.Log("Pause"); });
            Time.timeScale = (open) ? 0f : 1f;
        }


        // Update is called once per frame
        void Update()
        {

        }

    }
}
