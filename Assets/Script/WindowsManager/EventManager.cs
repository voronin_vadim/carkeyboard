﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;

public class EventManager : MonoSingleton<EventManager>
{


    Dictionary<string, UnityEvent> eventDictionary;
    public static void AddListener(Enum enuM, UnityAction listerer)
    {
        string name = enuM.ToString();
        AddListener(name, listerer);
    }

    public static void AddListener(string actionName, UnityAction listener)
    {
        UnityEvent thisEvent = null;
        if (Instance.eventDictionary.TryGetValue(actionName, out thisEvent))
        {
            thisEvent.AddListener(listener);
        }
        else
        {
            thisEvent = new UnityEvent();
            thisEvent.AddListener(listener);
            Instance.eventDictionary.Add(actionName, thisEvent);

        }
    }

    public static void RemoveActionListener(string name)
    {
        UnityEvent thisEvent = null;
        if (Instance.eventDictionary.TryGetValue(name, out thisEvent))
        {
            Instance.eventDictionary.Remove(name);

        }
    }
    public static bool IsContainEvents(string name)
    {
        if (name == null)
        {
            Debug.LogError("Event name == null");
            return false;
        }
        if (Instance.eventDictionary.ContainsKey(name))
            return true;
        return false;
    }

    public static void TriggerAction(Enum enu)
    {
        TriggerAction(enu.ToString());
    }
    public static void TriggerAction(string name)
    {
        if (!EventManager.IsContainEvents(name))
        {
            Debug.LogError($"Event manager do not contain Event with name <color = blue>{name} </color>");
            return;
        }
        UnityEvent thisEvent = null;
        if (Instance.eventDictionary.TryGetValue(name, out thisEvent))
        {
            thisEvent.Invoke();
        }
    }

    protected  void Awake()
    {
      //  base.Init();
        if (eventDictionary == null)
            eventDictionary = eventDictionary ?? new Dictionary<string, UnityEvent>();
        else
            eventDictionary.Clear();
    }
}
