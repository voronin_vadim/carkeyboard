﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
 
    

public class SettingManager : MonoBehaviour {

    [Header("Sound Button")]
    public Slider volymeSlider;
    public Toggle soundToogle;
    public Toggle musicToogle;

    [SerializeField]
    Button moreGame; 

    bool enableSound;
    bool enableMusic; 
    float soundValue;


 
    
    private void Start()
    {
        InitParam();
    }
   public  void OnOpen()  // on button
    {
    }
    void InitParam()
    {
        enableSound = SoundManager.SoundVolume > 0f;
        enableMusic = SoundManager.MusicVolume > 0f; 
      

        //SetSound(PlayerPrefs.GetFloat(VALUE, 1f));
      

        soundToogle.isOn = enableSound;
        soundToogle.onValueChanged.AddListener(SoundOn );
        SoundOn(enableSound); 

        musicToogle.isOn = enableMusic;
        musicToogle.onValueChanged.AddListener(MusicOn);
        MusicOn(enableMusic); 


        //slider
        if (volymeSlider)
        {
            volymeSlider.gameObject.SetActive(enableSound);
            volymeSlider.value = soundValue;
            volymeSlider.onValueChanged.AddListener(SoundChadge);
        }

        // more Game
        if (moreGame)
        moreGame.onClick.AddListener(MoreGame); 
    }
    public void SetEnglishLanguage(int lang)
    {
        string targ = ""; 
        switch (lang)

        {
            case 0:
                targ = "English"; 
                break;
            case 1:
                targ = "Russian";
                break;
            case 2:
                targ = "Espanol";
                break;
        }
        LocalizationService.Instance.Localization =targ; 
    }


    void MoreGame()
    {

    }
    void SoundChadge(float value)
    {
        SoundManager.SetVolumeSounds(value);
    }

    void MusicOn(bool value)
    {
        enableMusic = value; 
        SoundManager.SetVolumuMusic(value); 
    }
    void SoundOn( bool value)
    {
        enableSound = value; 
        SoundManager.SetVolumeSounds(value? 1f:0f); 
    }
  
}
