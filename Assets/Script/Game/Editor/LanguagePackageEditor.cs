﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;
using System.Text.RegularExpressions; 
     
    


[CustomEditor( typeof(LanguagePackage))]
public class LanguagePackageEditor : Editor {

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        var language = target as LanguagePackage;
       
        UpdateTextCount(); 
    }

    void UpdateTextCount()
    {
        var language = target as LanguagePackage;
        foreach (var text in language.text)
        {
            EditorGUILayout.LabelField("Text Name", text.name);
            EditorGUILayout.LabelField("Simbol count", text.text.Length.ToString());
           // Regex regex = new Regex(@"\s");
            Regex regex = new Regex(@"\n");
            EditorGUILayout.LabelField("Matches count   = ", regex.Matches(text.text).Count.ToString());
        }
    }



}
