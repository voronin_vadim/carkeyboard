﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Languege", menuName = "AddGameElem/AddLanguage")]
public class LanguagePackage : ScriptableObject {
   
   public   List<TextAsset> text;
    public string mainLanguage;


    public string GetRandomText()
    {
        return text.RandomItem().text;
    }
}
