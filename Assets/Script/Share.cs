﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Runtime.InteropServices;
public class Share : MonoBehaviour
{
    public string subject, ShareMessage, url;
    private bool isProcessing = false;
    public void ShareScreenshotWithText()
    {
        ShareSCr();
    }
    public void Start()
    {
    }
    public void ShareSCr()
    {
#if UNITY_ANDROID
        if (!isProcessing)
            StartCoroutine(ShareScreenshot());
#elif UNITY_IOS
if(!isProcessing)
StartCoroutine( CallSocialShareRoutine() );
#else
Debug.Log("No sharing set up for this platform.");
#endif
    }
#if UNITY_ANDROID
    public IEnumerator ShareScreenshot()
    {

        isProcessing = true;
        // wait for graphics to render
        yield return new WaitForEndOfFrame();

      
      //  ScreenCapture.CaptureScreenshot(ScreenshotName+".png");
        yield return new WaitForSeconds(0.1f);
        if (!Application.isEditor)
        {
            url = "https://play.google.com/store/apps/details?id=" + Application.identifier; 
            AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
            AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");
            intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
            AndroidJavaClass uriClass = new AndroidJavaClass("android.net.Uri");
           // AndroidJavaObject uriObject = uriClass.CallStatic<AndroidJavaObject>("parse", "file://" + screenShotPath);
           // intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_STREAM"), uriObject);
            intentObject.Call<AndroidJavaObject>("setType", "image/png");
            intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), url);
           // intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_HTML_TEXT"), url);
            AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");
            AndroidJavaObject jChooser = intentClass.CallStatic<AndroidJavaObject>("createChooser", intentObject, "");
            currentActivity.Call("startActivity", jChooser);
        }
        isProcessing = false;
    }
#endif

}
