﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.SceneManagement;



public class Cheat  {
   
    [MenuItem("HelpTools/ClearPlayerPrefs")]
    public static void Clear()
    {
        PlayerPrefs.DeleteAll();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex); 
    }
	
}
