﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq; 

public static  class ArrayExtension  {

    public static T RandomItem<T>(this IEnumerable<T> numerable )
    {
        int index = Random.Range(0, numerable.Count());
        return numerable.ElementAt(index); 
    }
	
}
