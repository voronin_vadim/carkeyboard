﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
public class Statistic : MonoBehaviour {

    public Text speedText;
    public Text completeText;
    public Text timerText;

    static int progress;
    static int timer;
    static int maxValue;
    static Action onValueChange = delegate { };


    public static int  Speed {
 get {
            return (progress * 60) / Mathf.Max(timer, 1); 
}
}
    const string spSuf = "SpeedSuf";  
    string speedSuf;

    public void Awake()
    {
        onValueChange += SetText; 
    }
    private void OnDestroy()
    {
        onValueChange -= SetText;
    }
    void Start()
    {
        onValueChange.Invoke();
        speedSuf= LocalizationService.Instance.GetTextByKey(spSuf);
    }

    public static  void SetParam(int? prog = null, int? tm = null ,int? max = null)
    {
        progress = prog ?? progress;
        timer = tm ?? timer;      
        maxValue = max ?? maxValue; 

        onValueChange.Invoke();
    }
   
    void SetText()
    {
        TimeSpan timerSP = TimeSpan.FromSeconds(timer);
        if (speedText) speedText.text = ((progress * 60) / Mathf.Max(timer, 1)).ToString() + speedSuf;
        if (completeText) completeText.text = (100 * progress / maxValue).ToString().ToString()  +"%";
        if (timerText) timerText.text = timerSP.Minutes.ToString("0") + ":" + timerSP.Seconds.ToString("00");
    }
    public static void Clear()
    {
        progress = 0;
        timer = 0;
        maxValue = 1; 
    }
}
