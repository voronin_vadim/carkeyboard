﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System; 


namespace Game2D.game
{
    public class PlayerResultShow : MonoBehaviour
    {


        [System.Serializable]
        public class ResultClass { }

        public static bool correctComplete;
        public static bool gameStarted;
        public static int resultPlace;
        public static int result;  
        public static string orderedNames; 

        //prefs const
        public const string GAMES = "GamesComplete";
        public const string WIN = "GamesWin";
        public const string BEST = "BestResult";
        public const string AVA = "Avarage"; 
        
        //  reward 
        List<int> placeRewardExp;
        List<int> placeReward;


        public Button shareResult;

        public int exitExp;
        public int exitCoin;

        [Header("UI")]
       
        public GameObject winObj;
        public GameObject loseObj;
        public GameObject incorectObj;
        public Text rewardText;
        public Text rewardTextExp;

        //stat text 
        public Text gameCompleteText;
        public Text bestResultText;
        public Text winText;
        public Text winPersentText;
        public Text avarageSpeedText; 
        

        //place
        [SerializeField]
        List<Text> textPlace;


        private void Awake()
        {
            placeReward = new List<int> { 20, 8, 4, 2 };
            placeRewardExp = new List<int> { 200, 50, 20, 5 };
        }

        private void Start()
        {
            correctComplete = GameManager.gameIsRightComplete;
            if (gameStarted) StartCoroutine(ShowRes());
            else
                ShowProfile(); 
            gameStarted = false;
            ShowProfile(); 
            shareResult.onClick.AddListener(() => ShareResult());

        }
        IEnumerator ShowRes()
        {
            yield return new WaitForSeconds(1f);
            ShowResult(); 
        }

        void ShowProfile()
        {
            int gameComplete = PlayerPrefs.GetInt(GAMES, 0);
            int win = PlayerPrefs.GetInt(WIN, 0);
            int bestRes = PlayerPrefs.GetInt(BEST, 0);
            float ava = PlayerPrefs.GetFloat(AVA, 0f);


            //Show common stat 

            gameCompleteText.text = gameComplete.ToString();
            bestResultText.text = bestRes.ToString();
            winText.text = win.ToString();
            winPersentText.text =(gameComplete==0)? "0": Mathf.CeilToInt((win / (float)gameComplete) * 100).ToString() + "%";
            avarageSpeedText.text = ava.ToString("0"); 

        }
        void ShareResult()
        {
            string gameLinq = "";
            string massage = LocalizationService.Instance.GetTextByKey("completeRace");
            string bestRes = LocalizationService.Instance.GetTextByKey("bestResult");

#if UNITY_ANDROID
            gameLinq = "https://play.google.com/store/apps/details?id=" + Application.identifier;
#elif UNITY_IPHONE

         gameLinq = "http://itunes.apple.com/app/id"; //+ appID.ToString();       
#endif

            // ButtonShare.Share(massage, gameLinq);
            
            new NativeShare().SetText(massage + " " + bestRes +": " + Statistic.Speed  + " "+ gameLinq).Share();
        }


        void ShowResult()
        {
          
            //correct complete
            Game2D.UI.Windows.ShowWindows("Win");
            textPlace.ForEach(p => p.transform.parent.gameObject.SetActive(false));
            incorectObj.SetActive(!correctComplete);

         

            if (!correctComplete)
            {
                Debug.LogError("Match do not complete");
                winObj.SetActive(false);
                loseObj.SetActive(false);

                SoundManager.PlaySound("lose");
                rewardText.text = "+" + exitCoin.ToString();
                rewardTextExp.text = "+" + exitExp.ToString();
                Shop.ShopController.AddMoney(exitCoin);
                Shop.ShopController.AddExpa(exitExp);

                return;
            }
          
            // int place = PlayerPrefs.GetInt(POS, 0);
            // string[] names = PlayerPrefs.GetString(NAMES, "").Split(new string []{ "///"} , StringSplitOptions.RemoveEmptyEntries);
            int place = resultPlace;                  
            string[] names =orderedNames.Split(new string []{ "///"} , StringSplitOptions.RemoveEmptyEntries);

            for (int i = 0; i < names.Length; i++)
            {
                textPlace[i].transform.parent.gameObject.SetActive(true);
                textPlace[i].text = names[i];
            }

       


          //  completeGO.SetActive(true);
            winObj.SetActive(place == 0);
            loseObj.SetActive(place != 0);
            SoundManager.PlaySound((place == 0) ? "win" : "lose");
            rewardText.text = "+" + placeReward[place].ToString();
            rewardTextExp.text = "+" + placeRewardExp[place].ToString();
            Shop.ShopController.AddMoney(placeReward[place]);
            Shop.ShopController.AddExpa(placeRewardExp[place]);

            // update stat    лучше всю стату в отдельный класс..
            int gameComplete = PlayerPrefs.GetInt(GAMES, 0);
            gameComplete++;
            PlayerPrefs.SetInt(GAMES, gameComplete);

            if (place == 0)
            {
                int win = PlayerPrefs.GetInt(WIN, 0);
                win++;
                PlayerPrefs.SetInt(WIN, win);
            }

            int bestRes = PlayerPrefs.GetInt(BEST, 0);
            int speed = Statistic.Speed; 
            if (speed > bestRes)
            {
                PlayerPrefs.SetInt(BEST, speed);
                bestRes = speed;
                GpsController.OnAddScoreToLeaderBorad(speed);
            }
            float ava = PlayerPrefs.GetFloat(AVA, 0f);
            ava = (gameComplete==0)? 0f: ava * (gameComplete-1) / (float)(gameComplete) + speed / (float)(gameComplete);
            PlayerPrefs.SetFloat(AVA, ava);
            PlayerPrefs.Save();

            ShowProfile();  
            StartCoroutine(ShowAds()); 
        }

        IEnumerator ShowAds()
        {
            yield return new WaitForSeconds(2f);
            AdShowManager.Instance.Show("win");
        }
    }
}
