﻿using UnityEngine;
using System.Collections;
using UnityEngine;
using System.Linq; 

public class RateUs : MonoBehaviour
{

#if UNITY_IOS
    public int appID;
#endif

    public GameObject[] starsOn;
    public GameObject[] starsOff;
    static bool openApp =false; 

    private void OnDisable()
    {
        for (int i = 0; i < 5; i++)
        {
            starsOn.ToList().ForEach(p=>p.SetActive(false));
            starsOff.ToList().ForEach(p => p.SetActive(false));
        }
    }

    public void Cancel()
    {
        gameObject.SetActive(false); 
    }

    public void Submit()
    {
#if UNITY_ANDROID
        Application.OpenURL("https://play.google.com/store/apps/details?id=" + Application.identifier);
#elif UNITY_IPHONE

        // Apple iTunes
         Application.OpenURL ("http://itunes.apple.com/app/id" + appID.ToString());
        
#endif

        //Cancel(); 
    }

    public void RateAsButton(int n)
    {
        for (int i = 0; i < n; i++)
        {
            starsOn[i].SetActive(true);
            starsOff[i].SetActive(false);
        }

        for (int i = n; i < 5; i++)
        {
            starsOn[i].SetActive(false);
            starsOff[i].SetActive(true);
        }

#if UNITY_ANDROID
       
            {
                // Google Review App Normal
            //    Application.OpenURL("market://details?id=" +Application.identifier);
          //  Application.OpenURL("https://play.google.com/store/apps/details?id=" + Application.identifier);
        }
     
#endif
    }
}
