﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;
    

public class SoundManager : MonoBehaviour {

   public  AudioClip mainMusic;
   public List<AudioClip> sounds;

    static   AudioSource musicSourse;
    static   List<AudioSource> soundSourse;

    static float musicVolume;
    static float soundVolume;
    public static float MusicVolume => musicVolume; 
    public static float SoundVolume => soundVolume;

    //PL prefs
    const string STATESOUND = "SoundState";
    const string STATEMUSIC = "MusicState";
    const string VALUE = "SoundValue";

    public static void SetVolumuMusic(bool enable)
    {
        if (!musicSourse) return;
        if (enable) musicSourse.UnPause();
        else musicSourse.Pause();
        PlayerPrefs.SetFloat(STATEMUSIC,enable? 1f:0f);
    }
    public static void SetVolumeSounds(float volume)
    {
        if (soundSourse!= null) soundSourse.ForEach(p => p.volume = volume);
        PlayerPrefs.SetFloat(STATESOUND, volume);
    }

    public static void PlaySound(string soundName)
    {
        AudioSource audio = soundSourse?.FirstOrDefault(p => p.name == soundName);
        if (audio == null)
        {
            Debug.LogWarning("Do not find audio with name " + soundName);
            return; 
        }
        audio.Play(); 
        
    }

    private void Awake()
    {
        soundVolume = PlayerPrefs.GetFloat(STATESOUND, 1f);
        musicVolume = PlayerPrefs.GetFloat(STATEMUSIC, 1f);

        if (soundSourse != null) return;
        CreateSounds();
    }
    private void OnLevelWasLoaded(int level)
    {
        if (!musicSourse) return;
        if (level == 0)
            musicSourse.UnPause();
        else musicSourse.Pause();
    }
    // Use this for initialization
    void Start () {

        GameObject canvas = GameObject.Find("Canvas");
        if (canvas == null) return;
        canvas.GetComponentsInChildren<Button>(true).ToList().ForEach(p => p.onClick.AddListener(() => PlaySound("click")));

          
    }


    void CreateSounds()
    {
        GameObject go;
        soundSourse = new List<AudioSource>();
        if (mainMusic)
        {
            go = new GameObject(mainMusic.name, (typeof(AudioSource)));
            DontDestroyOnLoad(go);
          //  go.transform.SetParent(this.transform);
            musicSourse = go.GetComponent<AudioSource>();
            musicSourse.clip = mainMusic;
            musicSourse.loop = true;
            musicSourse.Play();
        }
        for (int i = 0; i < sounds.Count; i++)
        {
            go = new GameObject(sounds[i].name, (typeof(AudioSource)));
            DontDestroyOnLoad(go); 
          //  go.transform.SetParent(this.transform);
            soundSourse.Add(go.GetComponent<AudioSource>());
            soundSourse[i].clip = sounds[i];
            soundSourse[i].playOnAwake = false;
        }
    }
}
