﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Runtime.InteropServices;
public class ButtonShare : MonoBehaviour
{
    public string subject, ShareMessage, url;
    private bool isProcessing = false;
    public string ScreenshotName = "logoIcon";

#if UNITY_IPHONE
    public struct ConfigStruct
    {
        public string title;
        public string message;
    }
    [DllImport("__Internal")] private static extern void showAlertMessage(ref ConfigStruct conf);
    public struct SocialSharingStruct
    {
        public string text;
        public string url;
    }
    [DllImport("__Internal")] private static extern void showSocialSharing(ref SocialSharingStruct conf);
    public void CallSocialShare(string title, string message)
    {
        ConfigStruct conf = new ConfigStruct();
        conf.title = title;
        conf.message = message;
        showAlertMessage(ref conf);
        isProcessing = false;
    }
#endif

    public static void Share(string massage, string url)
    {
        if (Application.isEditor)
        {
            Debug.Log("Share   in Editor");
            return;
        }
#if UNITY_ANDROID
        AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
        AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");
        intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
        if (!string.IsNullOrEmpty(url))
            intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), url);

        if (!string.IsNullOrEmpty(massage))
            intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), massage);

        AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");
        AndroidJavaObject jChooser = intentClass.CallStatic<AndroidJavaObject>("createChooser", intentObject, "");
        currentActivity.Call("startActivity", jChooser);
#endif
#if UNITY_IPHONE

        SocialSharingStruct conf = new SocialSharingStruct();
        conf.text = massage;
        conf.url = url;
        showSocialSharing(ref conf);
#endif
    }
}
