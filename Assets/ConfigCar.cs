﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName ="Car" , menuName = "AddGameElem/AddCar")]
public class ConfigCar : ScriptableObject {

    public Sprite mainSprite;
    public string carName;
    public Color fillColor; 
}
