﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;

public class NetworkMigrationManagerCustome : NetworkMigrationManager {
    public static NetworkMigrationManagerCustome Instance;
    private PeerInfoMessage m_NewHostInfo = new PeerInfoMessage();

    private void Awake()
    {
        print("Migration Awake call....");
        if (Instance == null) Instance = this;
        DontDestroyOnLoad(gameObject); 
    }

    public override bool FindNewHost(out PeerInfoMessage newHostInfo, out bool youAreNewHost)
    {
        print("FindNewHost");
        return base.FindNewHost(out newHostInfo, out youAreNewHost);
    }

    protected override void OnAuthorityUpdated(GameObject go, int connectionId, bool authorityState)
    {
        base.OnAuthorityUpdated(go, connectionId, authorityState);
        print("OnAuthorityUpdated");
    }

    protected override void OnClientDisconnectedFromHost(NetworkConnection conn, out SceneChangeOption sceneChange)
    {
        base.OnClientDisconnectedFromHost(conn, out sceneChange);
        print("OnClientDisconnectedFromHost");
        bool newhost;
        FindNewHost(out m_NewHostInfo, out newhost);
        print("New host :- " + newhost);

        if (newhost)
        {
            print("You become a new host at port :- " + NetworkManager.singleton.networkPort);
            BecomeNewHost(NetworkManager.singleton.networkPort);
        }

        else
        {
            Reset(this.oldServerConnectionId);
            waitingReconnectToNewHost = true;
            NetworkManager.singleton.networkAddress = m_NewHostInfo.address;
            NetworkManager.singleton.client.ReconnectToNewHost(m_NewHostInfo.address, NetworkManager.singleton.networkPort);
            print("You become a new client reconnect port -------> " + NetworkManager.singleton.networkPort);
            print("new server ip :- " + m_NewHostInfo.address);
        }
    }

    protected override void OnPeersUpdated(PeerListMessage peers)
    {
        base.OnPeersUpdated(peers);
    }

    protected override void OnServerHostShutdown()
    {
        base.OnServerHostShutdown();
    }

    protected override void OnServerReconnectObject(NetworkConnection newConnection, GameObject oldObject, int oldConnectionId)
    {
        base.OnServerReconnectObject(newConnection, oldObject, oldConnectionId);
        ReconnectObjectForConnection(newConnection, oldObject, oldConnectionId);
    }

    protected override void OnServerReconnectPlayer(NetworkConnection newConnection, GameObject oldPlayer, int oldConnectionId, short playerControllerId)
    {
        base.OnServerReconnectPlayer(newConnection, oldPlayer, oldConnectionId, playerControllerId);
        ReconnectPlayerForConnection(newConnection, oldPlayer, oldConnectionId, playerControllerId);
    }

    protected override void OnServerReconnectPlayer(NetworkConnection newConnection, GameObject oldPlayer, int oldConnectionId, short playerControllerId, NetworkReader extraMessageReader)
    {
        base.OnServerReconnectPlayer(newConnection, oldPlayer, oldConnectionId, playerControllerId, extraMessageReader);

    }
}



