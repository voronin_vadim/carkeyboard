﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Linq; 

namespace Game2D.Config
{

    public enum ConfigType
    {
        font, borders, cars, NONE 

    }

    public class ConfigManager : MonoSingleton<ConfigManager>
    {
       public  bool loadText; 

        //Differetn on each scene
        [SerializeField]
        List<Image> bordersImage;

        [SerializeField]
        List<Image> bordersImageSecond;


        [SerializeField]
        List<Text> chagetText;

        [SerializeField]
        List<Text> fontText;
        
        [SerializeField]
        List<InputField> field ; 

        [SerializeField]
        List<Image> playerCar;

        [SerializeField]
        List<Text> carName;

        //const string 
        public const string NICK_NAME = "StartInput";
        

        private void Start()
        {
             foreach (var type in Enum.GetValues(typeof(ConfigType)))
            {
                SetCurrentConfig((ConfigType)type);             
            }           
        }
        public void SetCurrentConfig(ConfigType type)
        {
            int value = PlayerPrefs.GetInt(type.ToString(), 0);
            SetConfig(type, value);
        }
    
        public static  int GetCurrentIndex(ConfigType type)
        {
          return   PlayerPrefs.GetInt(type.ToString(), 0);
        }
        public static string GetCurrentName()
        {
            return PlayerPrefs.GetString(NICK_NAME); 
        }
      

       public void ChangeConfig(ConfigType type, int number)
        {
            PlayerPrefs.SetInt(type.ToString(), number);
            PlayerPrefs.Save(); 
            SetConfig(type, number); 
        }

        void SetConfig(ConfigType type , int number)
        {

           
            var data = ConfigData.Instance.GetConfigType(type, number);
            switch (type)
            {
                case ConfigType.borders:
                        {
                        bordersImage.ForEach(p => p.sprite = ((ConfigBorder)data).mainSprite);
                        bordersImageSecond.ForEach(p => p.sprite = ((ConfigBorder)data).secondSprite);
                    }
                    break;
                case ConfigType.cars:
                    {playerCar.ForEach(p => p.sprite = ((ConfigCar)data).mainSprite);
                    carName.ForEach(p => p.text = ((ConfigCar)data).carName);
                        }
                    break;
                case ConfigType.font:
                    {
                        chagetText.ForEach(p => {
                            p.text = PlayerPrefs.GetString(NICK_NAME, ""); });

                        field.ForEach(k => k.text = PlayerPrefs.GetString(NICK_NAME, ""));
                        fontText.ForEach(p =>
                        {
                            p.font = ((ConfigFont)data).font;
                            p.fontSize = ((ConfigFont)data).fontsize;
                        }
                            ); 
                        

                    }
                    break;
            }
        }


    }
}
