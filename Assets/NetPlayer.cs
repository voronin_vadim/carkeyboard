﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Game2D.Config;
using UnityEngine.Networking;
using System;
using System.Linq; 
namespace Game2D.game {
    public class NetPlayer : Player {

        public delegate string LastSimDel(Player pl);

        public LastSimDel last;


        string lastText = ""; 
        string enterSimvol;
        string lastSimvol; 

        //identificator
        public int id; //test 
        public NetworkInstanceId plId; // test 
        public int index; 
             

        Coroutine keyBoard; 

        private TouchScreenKeyboard keyboard;
        public override void InitPlayer()
        {
           
            inGame = true;
        }
        private void Start()
        {
              
            Debug.Log("IN GAME INDEX " + index.ToString());

        }
        [ClientRpc]
       public void RpcSetStartIndex(int ind)
        {
            index = ind;
            Debug.Log(" NET PLAYER change index " + ind);
        }
        [ClientRpc]
        public void RpcSetName(string plName)
        {
            playerGameName = plName;
            Debug.Log(" NET PLAYER shange name "+ playerGameName); 
        }

      
        public override void OnStartClient()
        {
            TouchScreenKeyboard.hideInput = true;
            ShowKeyBoard();
            Debug.Log(" NET PLAYER Start Local Client");
        }

        public override void OnStartServer()
        {
            Debug.Log(" NET PLAYER OnStartServer ");

          //  SendStartParam();

        }
        [Command]
      public  void CmdSendStartParam()
        {          
            RpcSetStartIndex(index);  //track  player Index  
            Debug.Log("SET START PARAM" + index.ToString());
            RpcSetName(playerGameName);
        }

        public override void OnStartLocalPlayer()
        {
            base.OnStartLocalPlayer();
            Debug.Log("NET PLAYER Start Local Player"); 
        }

        public  void ShowKeyBoard()
        {
            keyboard = TouchScreenKeyboard.Open("", TouchScreenKeyboardType.Default, false, false, false, false, "");
        }
        public override void GameIsOver()
        {
            base.GameIsOver();// in game = false 
            if (keyBoard!= null) keyboard.active = false;
        }

        [Command]
        void CmdEnterSimbol(bool right)
        {
            RpcSimbolSend(right); // sendClient
          
        }

        [ClientRpc]
        public void RpcSimbolSend(bool result)
        {        
            Debug.Log("Hello " + "  RPS SEND " + isServer + isLocalPlayer);
            SendSimbol(this, result.ToString());
        }

        public void Update()
        {
            if (!inGame) return;
            if (!isLocalPlayer) return;

            if (lastText.Length != Input.inputString.Length)
            {
                enterSimvol = Input.inputString.Last().ToString();
                lastSimvol = last(this);
                CmdEnterSimbol(enterSimvol == lastSimvol);
                
            }
            /*
            if (Input.anyKeyDown)
            {
                enterSimvol = Input.inputString;
                string lastEnterSimvol = enterSimvol.Last().ToString(); 
                if (!String.IsNullOrEmpty(enterSimvol))
                {
                    lastSimvol = last(this);
                   // keyboard.text = "";
                    CmdEnterSimbol(lastEnterSimvol == lastSimvol);
                }
            }*/

           // TODO  как то нужно из евентов это получать
          

            if (keyboard.status != TouchScreenKeyboard.Status.Visible && keyBoard == null)
            {
                keyBoard = StartCoroutine(KeyBoardWait());
                ShowKeyBoard();
            }

        }
        IEnumerator KeyBoardWait()
        {
            yield return new WaitForSeconds(3f);
            keyBoard = null; 
                 
        }
    }
}
