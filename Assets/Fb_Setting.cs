﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Facebook.Unity;
using System.IO;
using System;
using UnityEngine.UI; 
public class Fb_Setting : MonoBehaviour
{
   public  Image playerImage; 

    bool waitForLog = true;


    public const string FB_NAME = "FaceBookName";

    [Header("Shered")]

    public string content;
    public string description;
    public string iconName;

    public static string fbName;
    // pbublic Action
    public static Action<bool> OnFbComplete = delegate { };


    public static bool firstInvoke  = true; 
    string path;
#if UNITY_IOS
   public int appID ; 
#endif

    private void Start()
    {
        if (!firstInvoke)
        { OnFbComplete(false); SetImage(); return; }

        firstInvoke = false; 
        if (!FB.IsInitialized)
            FB.Init(OnInitComplete, (p) => { });
        else
            OnInitComplete();
        SetImage();  

    }
    void OnInitComplete()
    {
        if (FB.IsInitialized)
        {
            FB.ActivateApp();
            if (FB.IsLoggedIn)
            {
                var aToken = AccessToken.CurrentAccessToken;
                Debug.Log($"USER ID <color=magenta>{aToken.UserId}</color>");
                FB.API("/me?fields=first_name", HttpMethod.GET, NameLog);
            }

            if (!FB.IsLoggedIn)
            {
                var permision = new List<string>() { "public_profile", "email"};
                FB.LogInWithReadPermissions(permision, OnLoginComplete);
            }

        }
        else { Debug.LogError("Can't FB init"); OnFbComplete(false); return; }
    }


    void OnLoginComplete(ILoginResult result)
    {
        if (result.Error != null)
        {
            Debug.LogError(result.Error);
            OnFbComplete(false);
           
            return;
        }
        if (FB.IsLoggedIn)
        {
            AccessToken accessToken = result.AccessToken;
            FB.API("/me?fields=first_name", HttpMethod.GET, NameLog);
            FB.API("/me/picture?type=normal", HttpMethod.GET, GetPicture);
        }
        OnFbComplete(false);
    }
    private void GetPicture(IGraphResult result)
    {
        if (result.Error == null)
        {
            var bytes = result.Texture.EncodeToPNG();
            File.WriteAllBytes(path, bytes);
            Debug.Log("Get Picture"); 
            SetImage(); 
        }

    }
    bool  SetImage()
    {
        path = Application.persistentDataPath + "/" + iconName + ".png";
        if (!File.Exists(path))
            return false ;
        var bytes = File.ReadAllBytes(path);
        Texture2D tex = new Texture2D(2, 2);
        tex.LoadImage(bytes);

        Sprite profileSpr =  Sprite.Create(tex ,  new Rect(Vector2.zero  , new Vector2(tex.width , tex.height)), Vector2.one*0.5f);
        playerImage.sprite = profileSpr;
        return true; 
    }


    

    void NameLog(IResult result)
    {
        if (result.Error != null)
        {
            Debug.LogError(result.Error);
            OnFbComplete(false);
            return;
        }
        string res = result.ResultDictionary["first_name"].ToString();
        PlayerPrefs.GetString(FB_NAME, res);
        fbName = res; 
        OnFbComplete(true);
        
    }

    private void OnApplicationQuit()
    {
        //if (FB.IsLoggedIn) FB.LogOut();
    }
    public void FacebookLogout()
    {
        FB.LogOut();
    }

    public void RateUs()
    {
        Application.OpenURL("https://play.google.com/store/apps/details?id=" + Application.identifier);
#if UNITY_IOS
       Application.OpenURL("http://itunes.apple.com/app/id" + appID.ToString()); 
#endif
    }

    public void Share()
    {
        System.UriBuilder uriBuilder = new System.UriBuilder(path);
        uriBuilder.Scheme = "file";
        Application.OpenURL(uriBuilder.ToString());

#if UNITY_ANDROID
        FB.ShareLink(new System.Uri("https://play.google.com/store/apps/details?id=" + Application.identifier), contentTitle: content,
            contentDescription: description, photoURL: uriBuilder.Uri);

#endif
#if UNITY_IOS
          FB.ShareLink(new System.Uri("http://itunes.apple.com/app/id" + appID.ToString()), contentTitle: content,
            contentDescription: description, photoURL:  uriBuilder.Uri);
#endif
    }



    void SetUpFaceBookParam()
    {

    }

}
