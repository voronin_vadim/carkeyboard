﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using DG.Tweening;
using System.Linq;
using System.Text;
using Random = UnityEngine.Random;
using Game2D.Config;
using UnityEngine.Networking;

namespace Game2D.game
{

    public class GameManager : MonoBehaviour
    {
        public static bool gameIsRightComplete = false; 
             

        public static bool localGame = true;
        public static bool inGame;

        string wiatText = "wait"; // localization
        string goText = "go"; //localization
        public Action OnStart = delegate { };
        public Text couldaunText;

        public Button showKeyBoard;
      

        [SerializeField]
        Net.PlayerLobbyShow[] playerShows;

        [SerializeField]
        List<Text> textPlace;
        List<string> playersName;


        [Header("ShowGameSetting")]
        public int needProgress;

        Scrollbar[] progressBars;


        IDictionary<Player, ProgressTextCheck> players;

        //String Builder
        string startString;
        public LibraryManager labrary;


        [Header("ShowText")]
        [SerializeField]
        Text currentText;
        ShowTextController show;

        // timer
        int currentTimerStep; 
        float startTimer;
        float currentTimer; 

        // Use this for initialization
        void Start()
        {
            gameIsRightComplete = false; 

            couldaunText.text = LocalizationService.Instance.GetTextByKey(wiatText);
            StartCountDown();
            showKeyBoard.onClick.AddListener(() => ShowKeyBoard());
           

            startString = labrary.GetText();
            startString = startString.Replace(ShowTextController.LINE_BREAK, " " + ShowTextController.LINE_BREAK);  // add empty to end string.
            show = new ShowTextController(startString, currentText);

            Statistic.Clear(); 
            // start Statistic 
            currentTimerStep = 0;          
            InitGame();
        }
        void ShowKeyBoard()
        {
            if (localGame)
                (players.Keys.FirstOrDefault(p => p is LocalPlayer) as LocalPlayer).ShowKeyBoard();
            else (players.Keys.FirstOrDefault(p => p.isLocalPlayer) as NetPlayer).ShowKeyBoard();
        }

        void InitGame()
        {
            playersName = new List<string>();
            progressBars = playerShows.Select(p => p.GetComponentInChildren<Scrollbar>()).ToArray();
            players = new Dictionary<Player, ProgressTextCheck>();
            foreach (var p in playerShows) p.gameObject.SetActive(false);
            Player player = null;
            Net.PlayerLobbyShow show;
            Statistic.SetParam(max: needProgress);
            // player Param
            int carIndex;
            int borderIndex;
            string namePlayer;

            if (localGame)
            {
                for (int i = 0; i < playerShows.Length; i++)
                {
                    show = playerShows[i];
                    show.gameObject.SetActive(true);
                    if (i == 0)
                    {
                        player = new GameObject("Player").AddComponent<LocalPlayer>();
                        carIndex = ConfigManager.GetCurrentIndex(ConfigType.cars);
                        borderIndex = ConfigManager.GetCurrentIndex(ConfigType.borders);
                        namePlayer = ConfigManager.GetCurrentName();

                    }
                    else
                    {
                        player = new GameObject("PlayerBot").AddComponent<PlayerBot>();
                        carIndex = ConfigData.Instance.GetRandom(ConfigType.cars);
                        borderIndex = ConfigData.Instance.GetRandom(ConfigType.borders);
                        namePlayer = "Bot " + i.ToString();
                    }


                    player.playerGameName = namePlayer;
                    ConfigBorder border = (ConfigBorder)ConfigData.Instance.GetConfigType(Config.ConfigType.borders, borderIndex);
                    ConfigCar car = (ConfigCar)ConfigData.Instance.GetConfigType(Config.ConfigType.cars, carIndex);

                    show.SetBorder(border.mainSprite);
                    show.SetCar(car.mainSprite, car.fillColor);
                    show.SetName(namePlayer);

                    players.Add(player, new ProgressTextCheck(startString.Replace(ShowTextController.LINE_BREAK, "").ToLower())); //  check progress only for clear text
                    player.SendSimbol += ResiveSimbol;
                }
            }
            else  // online game
            {
                NetworkLobbyPlayer[] pl = ((Net.LobbyManager)NetworkLobbyManager.singleton).lobbySlots;
                Net.PlayerLobby[] playersLlobby = pl.Where(p => p).Cast<Net.PlayerLobby>().ToArray();

                int playersCount = playersLlobby.Length;
                Net.PlayerLobby plLobby;
                for (int i = 0; i < playersCount; i++)
                {
                    show = playerShows[i];
                    show.gameObject.SetActive(true);

                    plLobby = playersLlobby[i];
                    carIndex = plLobby.spriteNum;
                    borderIndex = plLobby.borderNum;
                    namePlayer = plLobby.playerName;
                    playersName.Add(namePlayer);   // костыль на имена    потому как перебросить даные с лобби плеера на  игровой плеер можно только на хосте 

                    ConfigBorder border = (ConfigBorder)ConfigData.Instance.GetConfigType(Config.ConfigType.borders, borderIndex);
                    ConfigCar car = (ConfigCar)ConfigData.Instance.GetConfigType(Config.ConfigType.cars, carIndex);

                    show.SetBorder(border.mainSprite);
                    show.SetCar(car.mainSprite, car.fillColor);
                    show.SetName(namePlayer);

                    if (plLobby.isLocalPlayer)
                    {
                        Debug.Log("Local Player name = " + plLobby.name); 
                        show.gameObject.transform.SetSiblingIndex(0); 
                    }
                }
            }


        }
        void StartGame()
        {
            PlayerResultShow.gameStarted = true;  // проверяем что игра вообще запускалась
            if (!localGame)
            {
                NetPlayer[] allPl = FindObjectsOfType<NetPlayer>();
                allPl = allPl.OrderBy(p=>p.netId.Value).ToArray();//  to Do поправить  поиск игроков, а никак не поправить .....

                foreach (var pl in allPl)
                {
                    players.Add(pl, new ProgressTextCheck(startString.Replace(ShowTextController.LINE_BREAK, "").ToLower())); //  check progress only for clear text
                    pl.SendSimbol += ResiveSimbol;
                    pl.last = (GetRightSimvol);
                }
                for (int i = 0; i < allPl.Length; i++)
                {
                   // allPl[i].playerName = playersName[i]; // костыль на имена 
                }
            }


            players.Keys.ToList().ForEach(p => p.InitPlayer());
            startTimer = Time.realtimeSinceStartup;
            inGame = true;
        }
        public string GetRightSimvol(Player player)
        {
            string siv = players[player].GetRightSimvol();
            return siv;
        }

        //hadding resive simvol
        public void ResiveSimbol(Player pl, string sim)
        {
            bool localPlayer = pl is LocalPlayer;
            ProgressTextCheck textCheck = players[pl];
            int playerInd = players.Keys.ToList().IndexOf(pl);
           // int playerInd = playerShows.FindIndex(p => p.pName == pl.playerGameName);

            bool rightSim = false;

            if (pl is PlayerBot)
            {
                textCheck.EnterRightSimbol(Convert.ToBoolean(sim));
                rightSim = (Convert.ToBoolean(sim));
            }
            else if (pl is NetPlayer)
            {
                textCheck.EnterRightSimbol(Convert.ToBoolean(sim));
                rightSim = (Convert.ToBoolean(sim));
            }
            else
            {
                rightSim = textCheck.EnterSimbol(sim);
            }

            // if player make to mash mistake
            if (textCheck.overCome)
            {
                pl.SendSimbol -= ResiveSimbol;
                if (pl is LocalPlayer) //GameComplete(pl);
                    return;
            }

            List<int> progressList = textCheck.Progress;
            if (progressList == null)
            {
                Debug.LogError("Do not setting progress Player" + pl.ToString(), this);
                return;
            }

            int currentProgress = progressList.Where((p, i) => i % 2 == 0).Sum();
            if (currentProgress >= needProgress) GameComplete(pl);

            progressBars[playerInd].value = (currentProgress / (float)needProgress);
            if (localPlayer || pl.isLocalPlayer)
            {
                if (!rightSim) SoundManager.PlaySound("wrong");   
                show.ShowTextByProgress(progressList);
                Statistic.SetParam(prog: currentProgress); 
            }
        }





        //  if any is comlete then- game is over
        void GameComplete(Player pl)
        {
            inGame = false;
            players.Keys.ToList().ForEach(p => p.GameIsOver());

            int place = 0;
            //  find position by progress            
            var playerOrdered = players.OrderBy(p => p.Value.Progress.Where((i) => i % 2 == 0).Sum()).Reverse().Select(p => p.Key).ToList();
            place = playerOrdered.FindIndex(pos => pos is LocalPlayer || pos.isLocalPlayer);
            string orderedName = string.Join("///", playerOrdered.Select(p => p.playerGameName));


            PlayerResultShow.result =  players[playerOrdered[place]].Progress.Where(i => i % 2 == 0).Sum(); 
            PlayerResultShow.resultPlace = place;
            PlayerResultShow.orderedNames = orderedName; 
                     
            //clear Data
            var allPL = players.Keys;
            allPL.ToList().ForEach(p => Destroy(p.gameObject));
            players.Clear();



            gameIsRightComplete = true;
            Debug.Log("<color=green> GAME COMPLETE</color>");
            if (localGame)
                UnityEngine.SceneManagement.SceneManager.LoadScene("main"); 
            else
            Net.LobbyManager.l_Singleton?.StopClient();  // for test in game scene

        }

        public void ExitMenu()
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("main");
        }

      
        public void StartCountDown()
        {
            ShowStepCoultdaun(5);
        }

        void ShowStepCoultdaun(int num)
        {

            float fadeTimer = 1f;
            couldaunText.DOFade(1f, 0);
            couldaunText.transform.DOScale(Vector3.one * 1.5f, fadeTimer).OnComplete(() => { couldaunText.transform.localScale = Vector3.one; });
            couldaunText.DOFade(0, fadeTimer);

            if (num > 0)
            {
                couldaunText.text = num.ToString();
                SoundManager.PlaySound("timer");
            }
            else if (num == 0)
                couldaunText.text = LocalizationService.Instance.GetTextByKey(goText);
            else
            {
                couldaunText.gameObject.SetActive(false);
                couldaunText.transform.parent.parent.gameObject.SetActive(false);
                OnStart();
                StartGame();
                return;
            }
            num--;
            StartCoroutine(TimerCoroutine(1f, () => ShowStepCoultdaun(num)));
        }

        IEnumerator TimerCoroutine(float timer, Action act)
        {
            yield return new WaitForSeconds(timer);
            act.Invoke();
        }

        private void Update()
        {
            if (!inGame) return;

            currentTimer = Time.realtimeSinceStartup - startTimer;
            if (currentTimerStep <= currentTimer - 0.5) // update  rare 1second
            {
                currentTimerStep = (int)Mathf.Ceil(currentTimer);
                Statistic.SetParam(tm: currentTimerStep);             
            }

        }
    }
}

