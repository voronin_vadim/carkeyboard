﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Facebook.Unity;
using GameAnalyticsSDK;
using System; 


namespace Game2D.UI
{
    public class MenuManager : MonoBehaviour
    {

        public GameObject cover;
        public InputField input;
        public InputField startInput;

        //ConfigName

        public List<Text> playerName;
        GameObject windowsEnterName;

       
        
        // Use this for initialization
        void Start()
        {
            // Invoke("StartInput", 1f);

          //  AdShowManager.Instance.Show("start");
           
        }

        private void Awake()
        {
            GameAnalytics.Initialize();
            cover.SetActive(true);
            startInput.onEndEdit.AddListener((name) => { CompleteEnter(name);  });
            input.onEndEdit.AddListener((n) => InputComplete(n));
            Fb_Setting.OnFbComplete += FbComplete;
        }
        void FbComplete(bool fbSetName)
        {  
            cover.SetActive(false);
            if (PlayerPrefs.GetString(Config.ConfigManager.NICK_NAME, "") == "")
            {
                UI.Windows.ShowWindows(UI.NavigationButtonType.Input);
                startInput.text = Fb_Setting.fbName; 
                StartCoroutine(ShowInput(startInput));
            }
        }

        private void OnDestroy()
        {
            Fb_Setting.OnFbComplete -= FbComplete;
        }
        public void ChangeName()
        {
            StartCoroutine(ShowInput(input));
        }

        void InputComplete(string name)
        {
            PlayerPrefs.SetString(Config.ConfigManager.NICK_NAME, name);
            playerName.ForEach(p => p.text = name);
            Config.ConfigManager.Instance.SetCurrentConfig(Config.ConfigType.font); //ужас
            Debug.Log("StartInputComplete " + name);
        }

        public void LoadScene(string sceneName)
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(sceneName);
        }

        IEnumerator ShowInput(InputField field)
        {
            field.MoveTextStart(true);
            yield return new WaitForSeconds(0.5f);
            field.Select();
            //field.ActivateInputField();


        }
        void ShowField(InputField field)
        {
            field.MoveTextStart(true);
            field.Select(); 
        }

        void CompleteEnter(string nam)
        {
            if (String.IsNullOrEmpty(nam))
            {
                ShowField(startInput);
                return; 
            }
            InputComplete(nam);
            Windows.CloseCurentWindows();
        }

    }


}
